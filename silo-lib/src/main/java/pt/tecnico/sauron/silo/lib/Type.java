package pt.tecnico.sauron.silo.lib;

/* Enum containing all types known by Silo, Spotter and Eye
 * Enum types contain the appropriate RegEx to check
 * if their identifier is correct
 */
public enum Type {
    person("person", "^[1-9]+[0-9]*$", "^[1-9]+[*]?[0-9]*|[*][0-9]*$"),
    car("car", "^[A-Z]{4}[0-9]{2}|[0-9]{2}[A-Z]{4}|[A-Z]{2}[0-9]{2}[A-Z]{2}|[0-9]{4}[A-Z]{2}|[A-Z]{2}[0-9]{4}|[0-9]{2}[A-Z]{2}[0-9]{2}$", "^[A-Z0-9]*[*]?[A-Z0-9]+|[A-Z0-9]+[*]?[A-Z0-9]*$"),
    unknown("unknown", "", "");

    public final String name;
    public final String regex;
    public final String partial;

    private Type(String name, String regex, String partial) {
        this.name = name;
        this.regex = regex;
        this.partial = partial;
    }

    public String toString(){
        return this.name;
    }
}
