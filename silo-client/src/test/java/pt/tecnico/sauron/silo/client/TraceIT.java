package pt.tecnico.sauron.silo.client;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.*;
import pt.tecnico.sauron.silo.lib.Observation;
import pt.tecnico.sauron.silo.lib.SauronException;
import pt.tecnico.sauron.silo.lib.Camera;
import pt.tecnico.sauron.silo.lib.Type;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static pt.tecnico.sauron.silo.lib.SauronMessage.*;

public class TraceIT extends BaseIT {
    private static final String CAMERA_ID1 = "Camera0";
    private static final double CAMERA_LAT1 = 0;
    private static final double CAMERA_LONG1 = 0;
    private static final String CAMERA_ID2 = "Camera1";
    private static final double CAMERA_LAT2 = 10;
    private static final double CAMERA_LONG2 = -20;
    private static final Long PERSON_ID = 101010L;
    private static final String PERSON_INVALID_ID = "0A";
    private static final Long PERSON_UNREG_ID = 999999L;
    private static final String PERSON_PARTIAL_ID = "0*";
    private static final String CAR_ID = "AB0001";
    private static final String CAR_INVALID_ID = "AABBCC";
    private static final String CAR_UNREG_ID = "AB00BB";
    private static final String CAR_PARTIAL_ID = "ZZ*";

    private Camera camera1, camera2;
    private List<Observation> observations;

    @BeforeEach
    public void setUp() throws ZKNamingException, SauronException {
        frontend.ctrl_init();
        observations = new ArrayList<Observation>();
        camera1 = new Camera(CAMERA_ID1, CAMERA_LAT1, CAMERA_LONG1);
        camera2 = new Camera(CAMERA_ID2, CAMERA_LAT2, CAMERA_LONG2);
        observations.add(new Observation(Type.person, PERSON_ID, camera1));
        observations.add(new Observation(Type.car, CAR_ID, camera2));
        frontend.report(observations);
        observations.clear();
        observations.add(new Observation(Type.person, PERSON_ID, camera2));
        observations.add(new Observation(Type.car, CAR_ID, camera1));
        frontend.report(observations);
    }

    @AfterEach
    public void tearDown() throws ZKNamingException, SauronException {
        frontend.ctrl_clear();
    }

    @Test
    // Trace person good id exists
    public void tracePersonSuccessTest() throws ZKNamingException, SauronException {
        try {
            List<Observation> list = frontend.trail(Type.person, PERSON_ID.toString());
            assertEquals(2, list.size());
            Observation obs = list.get(0);
            assertEquals(camera2, obs.getCamera());
            assertEquals(Type.person, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(PERSON_ID, obs.getId());
            obs = list.get(1);
            assertEquals(camera1, obs.getCamera());
            assertEquals(Type.person, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(PERSON_ID, obs.getId());
        } catch (SauronException e) {
            fail("Sauron Exception thrown: " + e.getMessage());
        }
    }

    @Test
    // Test person bad id
    public void tracePersonInvalidId() throws ZKNamingException, SauronException {
        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.trail(Type.person, PERSON_INVALID_ID)).getMessage());
    }

    @Test
    // Test person good partial id
    public void tracePersonPartialId() throws ZKNamingException, SauronException {
        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.trail(Type.person, PERSON_PARTIAL_ID.toString()))
                        .getMessage());
    }

    @Test
    // Test person good id unregistered
    public void tracePersonUnregId() throws ZKNamingException, SauronException {
        assertEquals(ID_NOT_FOUND.message,
                assertThrows(SauronException.class, () -> frontend.trail(Type.person, PERSON_UNREG_ID.toString()))
                        .getMessage());
    }

    @Test
    // Test car good id exists
    public void traceCarSuccessTest() throws ZKNamingException, SauronException {
        try {
            List<Observation> list = frontend.trail(Type.car, CAR_ID);
            assertEquals(2, list.size());
            Observation obs = list.get(0);
            assertEquals(camera1, obs.getCamera());
            assertEquals(Type.car, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(CAR_ID, obs.getId());
            obs = list.get(1);
            assertEquals(camera2, obs.getCamera());
            assertEquals(Type.car, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(CAR_ID, obs.getId());
        } catch (SauronException e) {
            fail("Sauron Exception thrown: " + e.getMessage());
        }
    }

    @Test
    // Test car bad id
    public void traceCarInvalidId() throws ZKNamingException, SauronException {
        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.trail(Type.car, CAR_INVALID_ID)).getMessage());
    }

    @Test
    // Test car good partial id
    public void traceCarPartialId() throws ZKNamingException, SauronException {
        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.trail(Type.car, CAR_PARTIAL_ID)).getMessage());
    }

    @Test
    // Test car good id unregistered
    public void traceCarUnregId() throws ZKNamingException, SauronException {
        assertEquals(ID_NOT_FOUND.message,
                assertThrows(SauronException.class, () -> frontend.trail(Type.car, CAR_UNREG_ID)).getMessage());
    }
}
