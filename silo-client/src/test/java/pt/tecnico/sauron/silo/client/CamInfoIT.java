package pt.tecnico.sauron.silo.client;

import org.junit.jupiter.api.*;
import pt.tecnico.sauron.silo.lib.Camera;
import pt.tecnico.sauron.silo.lib.SauronException;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static pt.tecnico.sauron.silo.lib.SauronMessage.CAMERA_UNREGISTERED;

public class CamInfoIT extends BaseIT {

    Camera cam = new Camera("TestCamera", 10.000, 15.000);

    @Test
    public void OKCamInfo() throws ZKNamingException, SauronException {
        frontend.camJoin("TestCamera", 10.000, 15.000);
        assertEquals("(" + cam.getLatitude() + "," + cam.getLongitude() + ")", frontend.camInfo("TestCamera"));
    }

    @Test
    public void VoidCamInfo() throws ZKNamingException, SauronException {
        assertEquals(CAMERA_UNREGISTERED.message,
                assertThrows(SauronException.class, () -> frontend.camInfo("NotRegistered")).getMessage());
    }
}
