package pt.tecnico.sauron.silo.client;

import org.junit.jupiter.api.*;
import pt.tecnico.sauron.silo.grpc.*;
import pt.tecnico.sauron.silo.lib.SauronException;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static pt.tecnico.sauron.silo.lib.SauronMessage.*;

public class PingIT extends BaseIT {

    @Test
    public void pingOKTest() throws SauronException {
        String response = frontend.ctrl_ping("friend");
        assertEquals("Hello friend!", response);
    }

    @Test
    public void emptyPingTest() throws SauronException {
        assertEquals(SERVER_DISCONNECTED.message,
                assertThrows(SauronException.class, () -> frontend.ctrl_ping("")).getMessage());
    }

}
