package pt.tecnico.sauron.silo;

import java.time.Instant;
import java.util.List;
import pt.tecnico.sauron.silo.lib.Observation;
import pt.tecnico.sauron.silo.lib.Timestamp;

public class Record {
    private Timestamp timestamp;
    private List<Observation> arguments;
    private Instant instant;

    public Record() {
    }

	public Record(Timestamp timestamp, List<Observation> arguments, Instant instant) {
        this.timestamp = timestamp;
        this.arguments = arguments;
        this.instant = instant;
    }

    public List<Observation> getArguments() {
        return arguments;
    }

    public void setArguments(List<Observation> arguments) {
        this.arguments = arguments;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Instant getInstant() {
		return instant;
	}

	public void setInstant(Instant instant) {
		this.instant = instant;
	}

    public String toString() {
        return timestamp.getTimestamp() + ", " + arguments;
    }
}
