package pt.tecnico.sauron.silo;

import io.grpc.stub.StreamObserver;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.protobuf.Timestamp;

import pt.tecnico.sauron.silo.lib.Observation;
import pt.tecnico.sauron.silo.lib.Camera;
import pt.tecnico.sauron.silo.lib.InternalException;
import pt.tecnico.sauron.silo.lib.Type;
import pt.tecnico.sauron.silo.domain.Observable;
import pt.tecnico.sauron.silo.grpc.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import pt.ulisboa.tecnico.sdis.zk.ZKNaming;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;
import pt.ulisboa.tecnico.sdis.zk.ZKRecord;

import static io.grpc.Status.INVALID_ARGUMENT;
import static pt.tecnico.sauron.silo.lib.SauronMessage.*;

public class SiloServerImpl extends SiloGrpc.SiloImplBase {
    private List<Record> updateLog;
    private Map<Integer, pt.tecnico.sauron.silo.lib.Timestamp> tableTS;
    private Silo silo = new Silo();
    private Integer instance;
    private ZKNaming zookeeper;
    private String path;
    private static final int MAX_CHANNEL_SIZE = 1024 * 1024 * 1024; // 1GiB
    private boolean crashed = true;

    public SiloServerImpl(int instance, String host, String port, String path) {
        this.zookeeper = new ZKNaming(host, port);
        this.instance = instance;
        this.path = path;
        this.updateLog = new CopyOnWriteArrayList<Record>();
        this.tableTS = new ConcurrentHashMap<Integer, pt.tecnico.sauron.silo.lib.Timestamp>();
        this.tableTS.put(instance, new pt.tecnico.sauron.silo.lib.Timestamp());
    }

    /**
     * Gossip with other replicas
     */
    public void gossip() throws ZKNamingException {
        Collection<ZKRecord> zkrecords = this.zookeeper.listRecords(this.path);
        String URI = this.zookeeper.lookup(this.path + "/" + this.instance).getURI();

        for (ZKRecord record : zkrecords) {
            String target = record.getURI();

            if (!target.equals(URI)) {
                ManagedChannel channel = ManagedChannelBuilder.forTarget(target).usePlaintext().build();
                SiloGrpc.SiloBlockingStub stub = SiloGrpc.newBlockingStub(channel);

                TimestampData timeVec = convertToTimestampData(this.tableTS.get(this.instance));
                List<RecordData> recordsData = convertToRecordsData(this.updateLog);

                System.out.println("GOSSIPING WITH " + target + " " + this.updateLog.size() + " UPDATES");
                GossipResponse response = stub.gossip(GossipRequest.newBuilder().setTimeVec(timeVec)
                        .setReplica(this.instance).addAllRecords(recordsData).build());

                if (response.getCrash() && response.getTimeVec() != null) {
                    this.detectCrash(true, response.getReplica(), convertToTimestamp(response.getTimeVec()));
                }

                this.close(channel);
            }
            this.crashed = false;
        }

        if (zkrecords.size() == this.tableTS.size() && this.updateLog.size() != 0) {
            // Recycle updateLog
            List<Record> toRemove = new ArrayList<Record>();
            for (Record r : this.updateLog) {
                boolean remove = true;
                pt.tecnico.sauron.silo.lib.Timestamp rTs = r.getTimestamp();
                for (pt.tecnico.sauron.silo.lib.Timestamp t : this.tableTS.values()) {
                    if (t.happensBefore(rTs, this.instance)) {
                        remove = false;
                        break;
                    }
                }

                if (remove) {
                    toRemove.add(r);
                } else {
                    break;
                }
            }

            this.updateLog.removeAll(toRemove);
        }
    }

    /**
     * Receive messages from other replicas
     *
     * @param request          The request from other replica
     * @param responseObserver The stream where response will be sent
     */
    public void gossip(GossipRequest request, StreamObserver<GossipResponse> response) {
        pt.tecnico.sauron.silo.lib.Timestamp receivedTS = convertToTimestamp(request.getTimeVec());
        int replica = request.getReplica();

        this.detectCrash(replica, receivedTS);

        System.out.println("GOSSIP RECEIVED FROM REPLICA " + request.getReplica());

        if (this.tableTS.get(replica) != null
                && (receivedTS.getTimestamp().size() < this.tableTS.get(replica).getTimestamp().size()
                        || receivedTS.happensBefore(this.tableTS.get(replica)))) {
            System.out.println("CRASH DETECTED ON REPLICA " + replica);
            response.onNext(GossipResponse.newBuilder().setCrash(true)
                    .setTimeVec(convertToTimestampData(this.tableTS.get(this.instance))).setReplica(this.instance)
                    .build());
            response.onCompleted();
            return;
        }

        // Add received timestamp to tableTS
        this.tableTS.put(replica, receivedTS);

        // Merge timestamps if received TS > replicaTS
        if (this.tableTS.get(this.instance).happensBefore(receivedTS, replica)) {
            List<Record> receivedRecords = new ArrayList<>();
            try {
                receivedRecords = convertToRecords(request.getRecordsList());
                Collections.reverse(receivedRecords);
            } catch (InternalException e) {
                System.out.println(e.getMessage());
            }

            // Merge Records that aren't already in log

            for (Record record : receivedRecords) {
                pt.tecnico.sauron.silo.lib.Timestamp timestamp = record.getTimestamp();

                if (this.tableTS.get(this.instance).happensBefore(timestamp, replica)) {
                    // Add observations to Silo
                    this.silo.addObservations(record.getArguments(), record.getInstant());
                } else {
                    break;
                }
            }
            // Merge timestamps
            this.tableTS.get(this.instance).merge(receivedTS, replica);
        }

        response.onNext(GossipResponse.newBuilder().setCrash(false).build());
        response.onCompleted();

    }

    /**
     * Detect if replica crashed (no override check)
     *
     * @param replica   The replica number
     * @param timestamp The replica Timestamp
     */
    public void detectCrash(int replica, pt.tecnico.sauron.silo.lib.Timestamp timestamp) {
        this.detectCrash(false, replica, timestamp);
    }

    /**
     * Detect if replica crashed (with possible override)
     *
     * @param override  Override?
     * @param replica   The replica number
     * @param timestamp The replica Timestamp
     */
    public void detectCrash(boolean override, int replica, pt.tecnico.sauron.silo.lib.Timestamp timestamp) {
        synchronized (this.instance) {
            if (override || this.tableTS.get(this.instance).get(this.instance) < timestamp.get(this.instance)) {
                // connect to other replica
                System.out.println("CRASH DETECTED");
                System.out.println("ASKING FOR OBSERVATIONS ON REPLICA " + replica);
                this.crashed = true;
                try {
                    String target = this.zookeeper.lookup(this.path + "/" + replica).getURI();
                    ManagedChannel channel = ManagedChannelBuilder.forTarget(target)
                            .maxInboundMessageSize(this.MAX_CHANNEL_SIZE).usePlaintext().build();
                    SiloGrpc.SiloBlockingStub stub = SiloGrpc.newBlockingStub(channel);

                    // Deal with received info
                    List<ObservationData> observationData = stub.requestInfo(RequestInfoRequest.getDefaultInstance())
                            .getObsList();
                    List<Observation> observations = convertToObservations(observationData);
                    silo.clear();
                    silo.addCreatedObservations(observations);

                    // set new Timestamp
                    this.tableTS.put(this.instance, timestamp);

                    this.close(channel);

                    this.instance.notifyAll();
                } catch (ZKNamingException | InternalException e) {
                    System.out.println(e.getMessage());
                }
            }
            this.crashed = false;
        }
    }

    /**
     * Give requested info to other replica
     *
     * @param request          The request from other replica
     * @param responseObserver The stream where response will be sent
     *
     */
    public void requestInfo(RequestInfoRequest request, StreamObserver<RequestInfoResponse> responseObserver) {
        System.out.println("INFO REQUESTED");
        // put all observations into a list
        List<Observation> observations = silo.getObservationsList();
        List<ObservationData> observationData = convertToObservationsData(observations);
        // send them to the replica
        RequestInfoResponse response = RequestInfoResponse.newBuilder().addAllObs(observationData).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    /**
     * Check if server is alive
     *
     * @param request          The request from SiloFrontend
     * @param responseObserver The stream where response will be sent
     */
    public void ctrlPing(PingRequest request, StreamObserver<PingResponse> responseObserver) {
        String input = request.getText();
        if (input == null || input.isBlank()) {
            responseObserver.onError(INVALID_ARGUMENT.withDescription(EMPTY_INPUT.message).asRuntimeException());
        } else {
            String output = "Hello " + input + "!";
            PingResponse response = PingResponse.newBuilder().setText(output).build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }
    }

    /**
     * Clear server state
     *
     * @param request          The request from SiloFrontend
     * @param responseObserver The stream where response will be sent
     */
    public void ctrlClear(ClearRequest request, StreamObserver<ClearResponse> responseObserver) {
        System.out.println("CLEARED");
        responseObserver.onNext(ClearResponse.newBuilder().setText(this.silo.clear()).build());
        responseObserver.onCompleted();
    }

    /**
     * Initialize Server
     *
     * @param request          The request from SiloFrontend
     * @param responseObserver
     */
    public void ctrlInit(InitRequest request, StreamObserver<InitResponse> responseObserver) {
        responseObserver.onNext(InitResponse.newBuilder().setText(this.silo.init()).build());
        responseObserver.onCompleted();
    }

    /**
     * Register camera
     *
     * @param request          The request from SiloFrontend
     * @param responseObserver The stream where response will be sent
     */
    public void camJoin(CamJoinRequest request, StreamObserver<CamJoinResponse> responseObserver) {
        String name = request.getName();
        Double latitude = request.getLatitude();
        Double longitude = request.getLongitude();

        if (name == null || name.isBlank() || latitude == null || longitude == null) {
            System.out.println("TRIED TO CREATE INVALID CAMERA WITH NAME " + name + " AND COORDINATES (" + latitude
                    + "," + longitude + ")");
            responseObserver.onError(INVALID_ARGUMENT.withDescription(EMPTY_INPUT.message).asRuntimeException());
        } else {
            Camera camera;
            try {
                camera = silo.camJoin(name, latitude, longitude);
            } catch (InternalException e) {
                System.out.println("TRIED TO CREATE CAMERA WITH NAME " + name + " AND COORDINATES (" + latitude + ","
                        + longitude + ") EXCEPTION: " + e.getMessage());
                responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
                return;
            }
            CameraData camData = convertToCameraData(camera);
            CamJoinResponse response = CamJoinResponse.newBuilder().setCamData(camData).build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();

            System.out.println("CAMERA " + name + " CREATED WITH COORDINATES (" + latitude + "," + longitude + ")");
        }
    }

    /**
     * Get info of camera with given name
     *
     * @param request          The request from SiloFrontend
     * @param responseObserver The stream where response will be sent
     */
    public void camInfo(CamInfoRequest request, StreamObserver<CamInfoResponse> responseObserver) {
        String name = request.getName();

        if (name == null || name.isBlank()) {
            System.out.println("TRIED TO GET INFO FROM INVALID CAMERA WITH NAME " + name);
            responseObserver.onError(INVALID_ARGUMENT.withDescription(EMPTY_INPUT.message).asRuntimeException());
        } else {
            String coordinates;
            try {
                coordinates = silo.camInfo(name);
            } catch (InternalException e) {
                System.out.println(
                        "TRIED TO GET INFO FROM INVALID CAMERA WITH NAME " + name + "EXCEPTION: " + e.getMessage());
                responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
                return;
            }

            CamInfoResponse response;
            if (coordinates == "") {
                response = CamInfoResponse.newBuilder().setCamExists(false).build();
            } else {
                response = CamInfoResponse.newBuilder().setCamExists(true).setCoordinates(coordinates).build();
            }
            responseObserver.onNext(response);
            responseObserver.onCompleted();

            System.out.println("GOT INFO FROM CAMERA WITH NAME " + name);
        }
    }

    /**
     * Get the most recent observation for given entity
     *
     * @param request          The request from SiloFrontend
     * @param responseObserver The stream where response will be sent
     */
    public void track(TrackRequest request, StreamObserver<TrackResponse> responseObserver) {
        TrackResponse response;
        List<Observation> obsList = new ArrayList<Observation>();
        List<ObservationData> obsDataList;
        Pattern pattern;
        Matcher matcher;

        Type type = Type.unknown;
        switch (request.getObType().getNumber()) {
            case ObservableType.Person_VALUE:
                type = Type.person;
                try {
                    // id form check
                    checkType(Type.person, String.valueOf(request.getObId().getPerson()));
                    // silo function
                    obsList.add(silo.track(Type.person, request.getObId().getPerson()));
                } catch (InternalException e) {
                    System.out.println("TRIED TO GET " + type.name + " WITH ID " + request.getObId() + "EXCEPTION: "
                            + e.getMessage());
                    responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
                    return;
                }

                // convert and send back
                obsDataList = convertToObservationsData(obsList);
                response = TrackResponse.newBuilder().setTs(convertToTimestampData(this.tableTS.get(this.instance)))
                        .setObsData(obsDataList.get(0)).build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();
                break;

            case ObservableType.Car_VALUE:
                type = Type.car;
                try {
                    // id form check
                    checkType(Type.car, request.getObId().getCar());
                    // silo function
                    obsList.add(silo.track(Type.car, request.getObId().getCar()));
                } catch (InternalException e) {
                    System.out.println("TRIED TO TRACK " + type.name + " WITH ID " + request.getObId() + "EXCEPTION: "
                            + e.getMessage());
                    responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
                    return;
                }

                // convert and send back
                obsDataList = convertToObservationsData(obsList);
                response = TrackResponse.newBuilder().setTs(convertToTimestampData(this.tableTS.get(this.instance)))
                        .setObsData(obsDataList.get(0)).build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();
                break;

            default:
                System.out.println("TRIED TO TRACK UNKNOWN TYPE " + type.name + " WITH ID " + request.getObId());
                responseObserver.onError(INVALID_ARGUMENT.withDescription(UNKNOWN_TYPE.message).asRuntimeException());
                return;
        }

        System.out.println("TRACKED " + type.name + " WITH ID " + request.getObId());
    }

    /**
     * Get trace of entity with given id
     *
     * @param request          The resquest from SiloFrontend
     * @param responseObserver The stream where response will be sent
     */
    public void trace(TraceRequest request, StreamObserver<TraceResponse> responseObserver) {
        List<Observation> observations;
        List<ObservationData> observationDataList;
        TraceResponse response;
        Pattern pattern;
        Matcher matcher;

        Type type = Type.unknown;
        switch (request.getObType().getNumber()) {
            case ObservableType.Person_VALUE:
                type = Type.person;

                try {
                    // id form check
                    checkType(Type.person, String.valueOf(request.getObId().getPerson()));
                    // silo function
                    observations = silo.trace(Type.person, request.getObId().getPerson());
                } catch (InternalException e) {
                    System.out.println("TRIED TO TRACE " + type.name + " WITH ID " + request.getObId() + "EXCEPTION: "
                            + e.getMessage());
                    responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
                    return;
                }

                // sort list by timestamp
                observations.sort(Comparator.comparing(Observation::getTs).reversed());

                // convert and send back
                observationDataList = convertToObservationsData(observations);
                response = TraceResponse.newBuilder().setTs(convertToTimestampData(this.tableTS.get(this.instance)))
                        .addAllObsData(observationDataList).build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();
                break;

            case ObservableType.Car_VALUE:
                type = Type.car;

                try {
                    // id form check
                    checkType(Type.car, request.getObId().getCar());
                    // silo function
                    observations = silo.trace(Type.car, request.getObId().getCar());
                } catch (InternalException e) {
                    System.out.println("TRIED TO TRACE " + type.name + " WITH ID " + request.getObId() + "EXCEPTION: "
                            + e.getMessage());
                    responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
                    return;
                }

                // sort list by Timestamp
                observations.sort(Comparator.comparing(Observation::getTs).reversed());

                // convert and send back
                observationDataList = convertToObservationsData(observations);
                response = TraceResponse.newBuilder().setTs(convertToTimestampData(this.tableTS.get(this.instance)))
                        .addAllObsData(observationDataList).build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();
                break;

            default:

                System.out.println("TRIED TO TRACE UNKNOWN TYPE " + type.name + " WITH ID " + request.getObId());
                responseObserver.onError(INVALID_ARGUMENT.withDescription(UNKNOWN_TYPE.message).asRuntimeException());
                return;
        }

        System.out.println("TRACED " + type.name + " WITH ID " + request.getObId());
    }

    /**
     * Search for entities with given partial id
     *
     * @param request          The request from SiloFrontend
     * @param responseObserver The stream where response will be sent
     */
    public void trackMatch(TrackMatchRequest request, StreamObserver<TrackMatchResponse> responseObserver) {
        List<Observation> observations;
        List<ObservationData> observationDataList = new ArrayList<ObservationData>();
        TrackMatchResponse response;
        Pattern pattern;
        Matcher matcher;

        Type type = Type.unknown;
        switch (request.getObType().getNumber()) {
            case ObservableType.Person_VALUE:
                type = Type.person;

                try {
                    // id form check
                    checkPartialType(Type.person, request.getObId());
                    // silo function
                    observations = silo.trackMatch(Type.person, request.getObId());
                } catch (InternalException e) {
                    System.out.println("TRIED TO TRACK " + type.name + " WITH MATCHING ID " + request.getObId()
                            + "EXCEPTION: " + e.getMessage());
                    responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
                    return;
                }

                // convert and send back
                observationDataList = convertToObservationsData(observations);
                response = TrackMatchResponse.newBuilder()
                        .setTs(convertToTimestampData(this.tableTS.get(this.instance)))
                        .addAllObsData(observationDataList).build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();
                break;

            case ObservableType.Car_VALUE:
                type = Type.car;

                if (request.getObId().length() > 6) {
                    System.out
                            .println("TRIED TO TRACK " + type.name + " WITH MATCHING INVALID ID " + request.getObId());
                    responseObserver.onError(INVALID_ARGUMENT.withDescription(INVALID_ID.message).asRuntimeException());
                    return;
                }

                try {
                    // id form check
                    checkPartialType(Type.car, request.getObId());
                    // silo function
                    observations = silo.trackMatch(Type.car, request.getObId());
                } catch (InternalException e) {
                    System.out.println("TRIED TO TRACK " + type.name + " WITH MATCHING ID " + request.getObId()
                            + "EXCEPTION: " + e.getMessage());
                    responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
                    return;
                }

                // convert and send back
                observationDataList = convertToObservationsData(observations);
                response = TrackMatchResponse.newBuilder()
                        .setTs(convertToTimestampData(this.tableTS.get(this.instance)))
                        .addAllObsData(observationDataList).build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();
                break;

            default:
                System.out
                        .println("TRIED TO TRACK UNKNOWN TYPE " + type.name + " WITH MATCHING ID " + request.getObId());
                responseObserver.onError(INVALID_ARGUMENT.withDescription(UNKNOWN_TYPE.message).asRuntimeException());
                return;
        }

        System.out.println("TRACKED " + type.name + " WITH MATCHING ID " + request.getObId());
    }

    /**
     * Send observations to Silo
     *
     * @param request  The request from SiloFrontend
     * @param response The stream where response will be sent
     */
    public void report(ReportRequest request, StreamObserver<ReportResponse> response) {
        List<Observation> observations = new ArrayList<>();
        Instant instant = Instant.now();
        try {
            observations = convertToObservations(request.getObsList());
            silo.report(observations, instant);
        } catch (InternalException e) {
            System.out.println("TRIED TO REPORT " + observations.size() + " OBSERVATIONS EXCEPTION:" + e.getMessage());
            response.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());

            return;
        }

        response.onNext(ReportResponse.getDefaultInstance());
        response.onCompleted();

        if (this.crashed) {
            try {
                synchronized (this.instance) {
                    this.instance.wait();
                }
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }

        // update timestamp vector
        synchronized (this) {
            this.tableTS.get(this.instance).incrementUpdate(this.instance);

            // add update to update log
            this.updateLog.add(new Record(this.tableTS.get(this.instance).copy(), observations, instant));
        }

        System.out.println("REPORTED " + observations.size() + " OBSERVABLE ENTITIES");
    }

    /**
     * Convert GRPC timestamp to Sauron timestamp
     *
     * @param timeData The GRPC timestamp
     * @return The Sauron timestamp
     */
    public pt.tecnico.sauron.silo.lib.Timestamp convertToTimestamp(TimestampData timeData) {
        return new pt.tecnico.sauron.silo.lib.Timestamp(timeData.getVector());
    }

    /**
     * Convert GRPC list of records to Sauron list of records
     *
     * @param records The GRPC list of records
     * @return The Sauron list of records
     * @throws InternalException if converting to observations fails
     */
    public List<Record> convertToRecords(List<RecordData> records) throws InternalException {
        if (records == null) {
            throw new InternalException(RECORDS_NOT_TRANSMITTED);
        }
        List<Record> result = new ArrayList<Record>();
        for (RecordData rec : records) {
            if (rec == null) {
                throw new InternalException(RECORD_NOT_TRANSMITTED);
            }
            pt.tecnico.sauron.silo.lib.Timestamp timestamp = convertToTimestamp(rec.getTimestampData());
            if (timestamp == null) {
                throw new InternalException(TIMESTAMP_NOT_TRANSMITTED);
            }
            List<Observation> observations = convertToObservations(rec.getArgumentsList());
            Instant instant = convertToInstant(rec.getTs());

            if (instant == null) {
                throw new InternalException(INSTANT_NOT_TRANSMITTED);
            }
            Record record = new Record(timestamp, observations, instant);
            result.add(record);
        }

        return result;
    }

    /**
     * Convert Sauron list of records to GRPC list of records
     *
     * @param observations The Sauron list of records
     * @return The list of GRPC records
     */
    public List<RecordData> convertToRecordsData(List<Record> records) {
        List<RecordData> result = new ArrayList<RecordData>();
        for (Record rec : records) {

            RecordData record;
            RecordData.Builder builder = RecordData.newBuilder();

            List<ObservationData> arguments = convertToObservationsData(rec.getArguments());
            TimestampData timestampData = convertToTimestampData(rec.getTimestamp());
            Timestamp ts = convertToTimestamp(rec.getInstant());

            builder.setTimestampData(timestampData);
            builder.addAllArguments(arguments);
            builder.setTs(ts);

            record = builder.build();
            result.add(record);
        }

        return result;
    }

    /**
     * Convert GRPC camera to Sauron camera
     *
     * @param camData The GRPC camera
     * @return The Sauron camera
     */
    public Camera convertToCamera(CameraData camData) {
        return new Camera(camData.getName(), camData.getLatitude(), camData.getLongitude());
    }

    /**
     * Convert Sauron camera to GRPC camera
     *
     * @param camera The Sauron camera
     * @return The GRPC camera
     */
    public CameraData convertToCameraData(Camera camera) {
        return CameraData.newBuilder().setLatitude(camera.getLatitude()).setLongitude(camera.getLongitude())
                .setName(camera.getName()).build();
    }

    /**
     * Convert GRPC Timestamp to Instant
     *
     * @param timestamp The GRPC Timestamp
     * @return The Instanst
     */
    public Instant convertToInstant(Timestamp timestamp) {
        return Instant.ofEpochSecond(timestamp.getSeconds(), timestamp.getNanos());
    }

    /**
     * Convert Instant to GRPC Timestamp
     *
     * @param instant The Instant
     * @return The GRPC Timestamp
     */
    public Timestamp convertToTimestamp(Instant instant) {
        return Timestamp.newBuilder().setNanos(instant.getNano()).setSeconds(instant.getEpochSecond()).build();
    }

    /**
     * Convert GRPC list of observations to Sauron list of observations
     *
     * @param observations The GRPC list of observations
     * @return The Sauron list of observations
     * @throws InternalException if the one id is invalid for given type
     */
    public List<Observation> convertToObservations(List<ObservationData> observations) throws InternalException {
        List<Observation> result = new ArrayList<Observation>();
        for (ObservationData obs : observations) {
            Observation observation = new Observation();
            Pattern pattern;
            Matcher matcher;

            observation.setCamera(convertToCamera(obs.getCamera()));

            // Create to appropriate type
            switch (obs.getObType().getNumber()) {
                case ObservableType.Person_VALUE:
                    checkPartialType(Type.person, String.valueOf(obs.getObId().getPerson()));

                    observation.setId((Long) obs.getObId().getPerson());
                    observation.setType(Type.person);
                    break;
                case ObservableType.Car_VALUE:
                    checkPartialType(Type.car, obs.getObId().getCar());

                    observation.setId((String) obs.getObId().getCar());
                    observation.setType(Type.car);
                    break;
            }

            Timestamp ts = obs.getTs();
            // Convert timestamp to instant
            if (ts != null) {
                observation.setTs(convertToInstant(ts));
            }

            result.add(observation);
        }

        return result;
    }

    /**
     * Convert Sauron list of observations to GRPC list of observations
     *
     * @param observations The Sauron list of observations
     * @return The list of GRPC observations
     */
    public List<ObservationData> convertToObservationsData(List<Observation> observations) {
        List<ObservationData> result = new ArrayList<ObservationData>();
        for (Observation obs : observations) {

            ObservationData observation;
            ObservationData.Builder builder = ObservationData.newBuilder();
            builder.setCamera(convertToCameraData(obs.getCamera()));

            // Create to appropriate type
            switch (obs.getType().toString()) {
                case "person":
                    builder.setObType(ObservableType.Person);
                    builder.setObId(ObservableId.newBuilder().setPerson((Long) obs.getId()).build());
                    break;
                case "car":
                    builder.setObType(ObservableType.Car);
                    builder.setObId(ObservableId.newBuilder().setCar((String) obs.getId()));
                    break;
            }

            Instant instant = obs.getTs();
            // Convert instant to timestamp
            if (instant != null) {
                builder.setTs(convertToTimestamp(instant));
            }

            observation = builder.build();
            result.add(observation);
        }

        return result;
    }

    /**
     * Convert Sauron vector-timestamp to GRPC vector-timestamp
     *
     * @param time The Sauron vector-timestamp
     * @return The GRPC vector-timestamp
     */
    public TimestampData convertToTimestampData(pt.tecnico.sauron.silo.lib.Timestamp time) {
        return TimestampData.newBuilder().putAllVector(time.getTimestamp()).build();
    }

    /**
     * Check if id is valid for given type
     *
     * @param type The type of the entity
     * @param id   The id of the entity
     * @throws InternalException if requirements are not fullfilled
     */
    public void checkType(Type type, String id) throws InternalException {
        Pattern pattern = Pattern.compile(type.regex);
        Matcher matcher = pattern.matcher(id);

        if (!matcher.matches()) {
            throw new InternalException(INVALID_ID);
        }
    }

    /**
     * Check if id is partial for the given type
     *
     * @param type The type of the entity
     * @param id   The partial id of the entity
     * @throws InternalException if requirements are not fullfilled
     */
    public void checkPartialType(Type type, String id) throws InternalException {
        Pattern pattern = Pattern.compile(type.partial);
        Matcher matcher = pattern.matcher(id);

        if (!matcher.matches()) {
            throw new InternalException(INVALID_ID);
        }
    }

    /**
     * Close the channel to communicate with the server.
     *
     * @param channel channel to close
     */
    public void close(ManagedChannel channel) {
        try {
            while (!channel.shutdownNow().awaitTermination(30, TimeUnit.SECONDS))
                ;
        } catch (InterruptedException e) {
            System.out.println("Interrupted Exception while closing channel");
        }
    }
}
