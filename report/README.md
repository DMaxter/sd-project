# Relatório do projeto Sauron

Sistemas Distribuídos 2019-2020, segundo semestre


## Autores

**Grupo A19**

| Número | Nome              | Utilizador                           | Correio eletrónico                         |
| -------|-------------------|--------------------------------------| -------------------------------------------|
| 89428  | Daniel Serafim    | <https://github.com/dserafim1999>    | <mailto:daniel.serafim@tecnico.ulisboa.pt> |
| 89429  | Daniel Matos      | <https://github.com/DMaxter>         | <mailto:daniel.m.matos@tecnico.ulisboa.pt> |
| 89477  | João Paquete      | <https://github.com/jpaquete14>      | <mailto:joao.paquete@tecnico.ulisboa.pt>   |

<img src="assets/89428.jpeg" alt="89428" width="250"/><img src="assets/89429.jpeg" alt="89429" width="250"/><img src="assets/89477.jpeg" alt="89477" width="250"/>


## Melhorias da primeira parte

Redundâncias enviadas nos tipos GRPC foram retiradas
- [1](https://github.com/tecnico-distsys/A19-Sauron/commit/9028ef98743b1f3f861a0ee8020714d374a3164a)
- [2](https://github.com/tecnico-distsys/A19-Sauron/commit/43a9de2bbe3ea34e46430156c7e3ae2b4bccfccd)

Foram adicionados comentários mais extensivos:
- [1](https://github.com/tecnico-distsys/A19-Sauron/commit/798d87710338d1d3d5910fe19bce285cc8134336)
- [2](https://github.com/tecnico-distsys/A19-Sauron/commit/6061c594fb748dec8cba1f03049cb0d2f4b5b7ac)
- [3](https://github.com/tecnico-distsys/A19-Sauron/commit/b990553bdd6d1ddeef921bac7ba1a875d514bd94)
- [4](https://github.com/tecnico-distsys/A19-Sauron/commit/0c69f67d0f06bd192de075fb5ec85e3f720b35fb)

Foram adicionados testes mais extensivos para cada bloco:
- [Testes T1](https://github.com/tecnico-distsys/A19-Sauron/commit/c53262c23c6609a7b8a8449b420d7000aef18464)
- Testes T2
  - [1](https://github.com/tecnico-distsys/A19-Sauron/commit/94f4b234a218a7c98e5625a9447d51833f5d7d41)
  - [2](https://github.com/tecnico-distsys/A19-Sauron/commit/6061c594fb748dec8cba1f03049cb0d2f4b5b7ac)
- [Testes T3](https://github.com/tecnico-distsys/A19-Sauron/commit/c5110a547f4c44e3658b62bb91d24f1bbc1e67ad)
- [O ctrl_init do Cliente agora insere mais câmaras para suportar estes novos testes](https://github.com/tecnico-distsys/A19-Sauron/commit/94f4b234a218a7c98e5625a9447d51833f5d7d41)


## Modelo de faltas

### Faltas não toleradas

- Servidor de nomes *crashar*
- Todas as réplicas *crasharem*
- Cliente tentar conectar diretamente a uma réplica inexistente/"morta"
- Chamada remota abortar na réplica sem enviar exceção

### Faltas toleradas

- Todas as réplicas menos 1 *crasharem* (parcialmente, ver [aqui](#impl))
- Não existirem réplicas para realizar Gossip
- Mensagens de Gossip serem perdidas na rede
- Mensagens de Gossip chegarem com erros
- Réplica ter uma vista anterior à do cliente (parcialmente, ver [aqui](#impl))
- Cliente tentar conectar a uma réplica que funcionava corretamente e que *crashou*
- Cliente tentar conectar a uma réplica que *crashou* e voltou a funcionar
- Chamada remota abortar em caso de *crash* da réplica ou envio de exceção pelo canal
- Cameras com nomes iguais, coordenadas diferentes em réplicas diferentes


## Solução

<img src="assets/solucao.png"/>

Inicialmente ligados vários servidores (idealmente mais do que 1), e vão trocando mensagens entre si.

Se um eye se conectar, pede ao servidor de nomes (ZooKeeper) a localização (target) para uma réplica específica, ou pede a lista de todos e escolhe um qualquer.

O eye envia as observações diretamente para a réplica.

O spotter para se conectar procede da mesma maneira.

O spotter executa um pedido na réplica, recebe a resposta, no frontend compara com o que tem na cache, e retorna ao cliente vista adequada.

 Se ao tentar realizar um pedido, a réplica que estava a contactar falhar, o frontend pede ao servidor de nomes todas as réplicas ativas e escolhe uma para contactar.

 No caso de uma mensagem de Gossip falhar, continuamos a executar o protocolo Gossip, e (otimisticamente) esperamos que não falhe na próxima ronda de Gossip (se falhar, continuamos sempre a executar). Os updates só serão eliminados quando eventualmente essa réplica transmitir às outras que os recebeu, através do seu timestamp, como descrito [aqui](#impl).


## Protocolo de replicação

### Explicação do protocolo

O protocolo é uma adaptação do protocolo de comunicação de coerência fraca, o *gossip*. Este protocolo permite oferecer sempre acesso rápido aos clientes, mesmo em situações de partições, sacrificando a sua coerência. Os clientes enviam pedidos de leitura ou modificação a uma réplica que propaga periodicamente essas modificações de forma relaxada, podendo ter vistas divergentes. Apesar de se tratar de um protocolo com coerência fraca, os valores que um cliente lê são coerentes entre si, isto é, um cliente nunca vê uma vista mais antiga do que a que já viu.

Estas particularidades do protocolo *gossip* mantêm-se verdade na nossa implementação. Na nossa versão, apenas existe uma operação de *update* , o **report** , o que nos permite simplificar alguns aspetos do protocolo original. Além disso, cada cliente possui uma cache, que permite, em caso de *query*, garantir que o cliente apresenta sempre o valor mais atualizado que já recebeu. A comunicação é realizada entre **TODAS** as réplicas que se encontram ativas no momento da troca de mensagens, o que permite simplificar o update log. Para "reciclarmos" o updateLog, usamos uma estratégia parecida à do protocolo original, remover quando todas as réplicas reportaram à réplica que tem os updates, o timestamp vetorial atualizado com os updates enviados.

### Trocas de mensagens

As trocas de mensagens de uma réplica realizam-se numa thread que é iniciada, por predefinição, de 30 em 30 segundos, podendo este valor ser alterado. Cada vez que se dá inicio à comunicação, essa réplica itera sobre todas as réplicas que se encontram ativas. Para cada uma dessas réplicas, é comparado o timestamp vetorial de updates. Se a réplica a realizar a comunicação tem alguma réplica desatualizada no seu timestamp vetorial em comparação à réplica com quem está a comunicar, é feito um merge dos seus timestamps, bem como do seu update log. Se não tiver nenhuma réplica desatualizada (podendo até ter réplicas mais atualizadas que as restantes), não acontece nada.


## <a name="impl">Opções de implementação</a>

- Fazer **ping** à réplica antes de cada pedido, para verificar se esta está em baixo ou não e assim mudar/ou não de réplica
- Cache no frontend com política de remoção LRU (previne a maior parte dos problemas de leitura coerente), se a vista da cache for mais recente que a da réplica, retorna a da cache, caso contrário ou não exista é substituída/adicionada a vista na cache e mostra essa vista ao cliente
- Gossip com todas as réplicas, por serem em número reduzido
- Update log com entradas do tipo <timestamp vetorial, updates (lista de observações), timestamp (ISO 8601)>, porque cada réplica comunica com todas as outras que estão ativas
- Gossip assim que a réplica fica ativa, para verificar se a réplica *crashou* ou se é uma réplica nova, permitindo no primeiro caso recuperar todos os updates que a réplica contactada tiver, apenas perdendo os que a réplica tinha que não tinham sido propagados (reports estão bloqueados até a réplica considerar que não *crashou*)
- Cameras com nomes iguais e coordenadas diferentes em réplicas diferentes são permitidas, mas "chocam" quando uma delas muda para a réplica da outra. Neste caso, a camera que já se encontrava na réplica mantém-se, e a outra muda para essa réplica

## Notas finais

- Para correr os testes, assume-se que é a réplica 1 que está ativa, podem haver outras, mas vão estar a trocar mensagens de Gossip, por isso não é muito aconselhável
- Como se trata de um projeto meramente académico, e também por sugestão do docente, decidimos fazer **gossip** com todas as réplicas ativas, sendo que otimamente deveria ser realizado apenas para um número limitado de réplicas (metade do número de réplicas + 1, por exemplo).
- Também optámos, por sugestão do docente, por não fazer **gossip** das câmeras entre réplicas, simplificando ainda mais o nosso protocolo
