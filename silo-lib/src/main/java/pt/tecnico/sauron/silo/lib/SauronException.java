package pt.tecnico.sauron.silo.lib;

public class SauronException extends Exception{
    private static final long serialVersionUID = 20200328L;

    public SauronException(SauronMessage message) {
        super(message.message);
    }

    public SauronException(String message){
        super(message);
    }
}
