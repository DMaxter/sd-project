package pt.tecnico.sauron.silo.client;

import java.util.List;
import java.util.ArrayList;

import org.junit.jupiter.api.*;
import pt.tecnico.sauron.silo.lib.Observation;
import pt.tecnico.sauron.silo.lib.SauronException;
import pt.tecnico.sauron.silo.lib.Camera;
import pt.tecnico.sauron.silo.lib.Type;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static pt.tecnico.sauron.silo.lib.SauronMessage.*;

public class TrackIT extends BaseIT {
    private static final String CAMERA_ID = "Camera0";
    private static final double CAMERA_LAT = 0;
    private static final double CAMERA_LONG = 0;
    private static final Long PERSON_ID1 = Long.MAX_VALUE;
    private static final Long PERSON_ID2 = 2290L;
    private static final Long PERSON_INVALID_ID = 0L;
    private static final Long PERSON_NEG_ID = -1L;
    private static final Long PERSON_UNREG_ID = 10000L;
    private static final String PERSON_PARTIAL_BEG = "*0";
    private static final String PERSON_PARTIAL_MID = "2*0";
    private static final String PERSON_PARTIAL_END = "22*";
    private static final String PERSON_PARTIAL_INVALID_ID = "0A*";
    private static final String PERSON_PARTIAL_UNREG_ID = "999*";
    private static final String CAR_ID1 = "AB0001";
    private static final String CAR_ID2 = "AB0003";
    private static final String CAR_ID3 = "AA0003";
    private static final String CAR_INVALID_LEN_ID = "00AA00AA";
    private static final String CAR_UNREG_ID = "ZZZZ99";
    private static final String CAR_PARTIAL_BEG = "*03";
    private static final String CAR_PARTIAL_MID = "A*03";
    private static final String CAR_PARTIAL_END = "AB*";
    private static final String CAR_PARTIAL_INVALID_ID = "AAAAAA*";
    private static final String CAR_PARTIAL_INVALID_LEN_ID = "00AA*00AA";
    private static final String CAR_PARTIAL_UNREG_ID = "ZZ*";

    private List<Observation> observations;
    private Camera camera;

    @BeforeEach
    public void setUp() throws ZKNamingException, SauronException {
        frontend.ctrl_init();
        observations = new ArrayList<Observation>();
        camera = new Camera(CAMERA_ID, CAMERA_LAT, CAMERA_LONG);
        observations.add(new Observation(Type.person, PERSON_ID1, camera));
        observations.add(new Observation(Type.person, PERSON_ID2, camera));
        observations.add(new Observation(Type.car, CAR_ID1, camera));
        observations.add(new Observation(Type.car, CAR_ID2, camera));
        observations.add(new Observation(Type.car, CAR_ID3, camera));

        try {
            frontend.report(observations);
        } catch (SauronException e) {
            fail("Sauron Exception thrown: " + e.getMessage());
        }
    }

    @AfterEach
    public void tearDown() throws ZKNamingException, SauronException {
        frontend.ctrl_clear();
    }

    // Test person good id
    @Test
    public void trackPersonSuccessTest() throws ZKNamingException, SauronException {
        try {
            List<Observation> list = frontend.spot(Type.person, PERSON_ID1.toString());
            assertEquals(1, list.size());
            Observation obs = list.get(0);
            assertEquals(camera, obs.getCamera());
            assertEquals(Type.person, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(PERSON_ID1, obs.getId());
        } catch (SauronException e) {
            fail("Sauron Exception thrown: " + e.getMessage());
        }
    }

    // Test person bad id
    @Test
    public void trackPersonWrongIdFormat() throws ZKNamingException, SauronException {
        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.spot(Type.person, PERSON_INVALID_ID.toString()))
                        .getMessage());
    }

    // Test person bad id
    @Test
    public void trackPersonNegativeId() throws ZKNamingException, SauronException {
        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.spot(Type.person, PERSON_NEG_ID.toString()))
                        .getMessage());
    }

    // Test person unregistered id
    @Test
    public void trackPersonUnregisteredId() throws ZKNamingException, SauronException {
        assertEquals(ID_NOT_FOUND.message,
                assertThrows(SauronException.class, () -> frontend.spot(Type.person, PERSON_UNREG_ID.toString()))
                        .getMessage());
    }

    // Test car good id
    @Test
    public void trackCarSuccessTest() throws ZKNamingException, SauronException {
        try {
            List<Observation> list = frontend.spot(Type.car, CAR_ID1);
            assertEquals(1, list.size());
            Observation obs = list.get(0);
            assertEquals(camera, obs.getCamera());
            assertEquals(Type.car, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(CAR_ID1, obs.getId());
        } catch (SauronException e) {
            fail("Sauron Exception thrown: " + e.getMessage());
        }
    }

    // Test car bad id
    @Test
    public void trackCarInvalidLenId() throws ZKNamingException, SauronException {
        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.spot(Type.car, CAR_INVALID_LEN_ID)).getMessage());
    }

    // Test car unregistered id
    @Test
    public void trackCarUnregId() throws ZKNamingException, SauronException {
        assertEquals(ID_NOT_FOUND.message,
                assertThrows(SauronException.class, () -> frontend.spot(Type.car, CAR_UNREG_ID)).getMessage());
    }

    // Test match beginning person good id
    @Test
    public void trackMatchPersonBegSuccessTest() throws ZKNamingException, SauronException {
        try {
            List<Observation> list = frontend.spot(Type.person, PERSON_PARTIAL_BEG);
            assertEquals(1, list.size());
            Observation obs = list.get(0);
            assertEquals(camera, obs.getCamera());
            assertEquals(Type.person, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(PERSON_ID2, obs.getId());
        } catch (SauronException e) {
            fail("Sauron Exception thrown: " + e.getMessage());
        }
    }

    // Test match middle person good id
    @Test
    public void trackMatchPersonMidSuccessTest() throws ZKNamingException, SauronException {
        try {
            List<Observation> list = frontend.spot(Type.person, PERSON_PARTIAL_MID);
            assertEquals(1, list.size());
            Observation obs = list.get(0);
            assertEquals(camera, obs.getCamera());
            assertEquals(Type.person, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(PERSON_ID2, obs.getId());
        } catch (SauronException e) {
            fail("Sauron Exception thrown: " + e.getMessage());
        }
    }

    // Test match end person good id
    @Test
    public void trackMatchPersonEndSuccessTest() throws ZKNamingException, SauronException {
        try {
            List<Observation> list = frontend.spot(Type.person, PERSON_PARTIAL_END);
            assertEquals(1, list.size());
            Observation obs = list.get(0);
            assertEquals(camera, obs.getCamera());
            assertEquals(Type.person, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(PERSON_ID2, obs.getId());
        } catch (SauronException e) {
            fail("Sauron Exception thrown: " + e.getMessage());
        }
    }

    // Test match person bad id
    @Test
    public void trackMatchPersonInvalidFormatId() throws ZKNamingException, SauronException {
        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.spot(Type.person, PERSON_PARTIAL_INVALID_ID))
                        .getMessage());
    }

    // Test match person unregistered id
    @Test
    public void trackMatchPersonUnregId() throws ZKNamingException, SauronException {
        assertEquals(ID_NOT_FOUND.message,
                assertThrows(SauronException.class, () -> frontend.spot(Type.person, PERSON_PARTIAL_UNREG_ID))
                        .getMessage());
    }

    // Test match beginning car good id
    @Test
    public void trackMatchCarBegSuccessTest() throws ZKNamingException, SauronException {
        try {
            List<Observation> list = frontend.spot(Type.car, CAR_PARTIAL_BEG);
            assertEquals(2, list.size());
            Observation obs = list.get(0);
            assertEquals(camera, obs.getCamera());
            assertEquals(Type.car, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(CAR_ID3, obs.getId());
            obs = list.get(1);
            assertEquals(camera, obs.getCamera());
            assertEquals(Type.car, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(CAR_ID2, obs.getId());
        } catch (SauronException e) {
            fail("Sauron Exception thrown: " + e.getMessage());
        }
    }

    // Test match middle car good id
    @Test
    public void trackMatchCarMidSuccessTest() throws ZKNamingException, SauronException {
        try {
            List<Observation> list = frontend.spot(Type.car, CAR_PARTIAL_MID);
            assertEquals(2, list.size());
            Observation obs = list.get(0);
            assertEquals(camera, obs.getCamera());
            assertEquals(Type.car, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(CAR_ID3, obs.getId());
            obs = list.get(1);
            assertEquals(camera, obs.getCamera());
            assertEquals(Type.car, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(CAR_ID2, obs.getId());
        } catch (SauronException e) {
            fail("Sauron Exception thrown: " + e.getMessage());
        }
    }

    // Test match end car good id
    @Test
    public void trackMatchCarEndSuccessTest() throws ZKNamingException, SauronException {
        try {
            List<Observation> list = frontend.spot(Type.car, CAR_PARTIAL_END);
            assertEquals(2, list.size());
            Observation obs = list.get(0);
            assertEquals(camera, obs.getCamera());
            assertEquals(Type.car, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(CAR_ID1, obs.getId());
            obs = list.get(1);
            assertEquals(camera, obs.getCamera());
            assertEquals(Type.car, obs.getType());
            assertNotEquals(null, obs.getTs());
            assertEquals(CAR_ID2, obs.getId());
        } catch (SauronException e) {
            fail("Sauron Exception thrown: " + e.getMessage());
        }
    }

    // Test match car bad id
    @Test
    public void trackMatchCarInvalidLenId() throws ZKNamingException, SauronException {
        assertEquals(INVALID_ID.message, assertThrows(SauronException.class,
                () -> frontend.spot(Type.car, CAR_PARTIAL_INVALID_LEN_ID.toString())).getMessage());
    }

    // Test match car bad id
    @Test
    public void trackMatchCarInvalidFormatId() throws ZKNamingException, SauronException {
        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.spot(Type.car, CAR_PARTIAL_INVALID_ID.toString()))
                        .getMessage());
    }

    // Test match car unregistered id
    @Test
    public void trackMatchCarUnregId() throws ZKNamingException, SauronException {
        assertEquals(ID_NOT_FOUND.message,
                assertThrows(SauronException.class, () -> frontend.spot(Type.car, CAR_PARTIAL_UNREG_ID)).getMessage());
    }
}
