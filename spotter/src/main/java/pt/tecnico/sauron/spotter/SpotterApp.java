package pt.tecnico.sauron.spotter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import pt.tecnico.sauron.silo.lib.SauronException;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;

public class SpotterApp {
    public static void main(String[] args) throws IOException, ZKNamingException {
        System.out.println(SpotterApp.class.getSimpleName());

        if (args.length < 4 || args.length > 5) {
            System.out.println("Error: invalid usage");
        }

        final String zooHost = args[0];
        final String zooPort = args[1];
        final String path = args[2];
        final int cacheSize = Integer.parseInt(args[3]);
        String instance = null;

        if (args.length == 5) {
            instance = args[4];
        }

        Spotter spotter = new Spotter(zooHost, zooPort, path, instance, cacheSize);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        // Shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            spotter.close();
        }));

        while (true) {
            System.out.print("> ");
            String input = reader.readLine();

            if (input == null) {
                System.out.println();
                break;
            }

            try {
                parse(input, spotter);
            } catch (SauronException e) {
                System.out.printf("%s: %s%n", e.getStackTrace()[0].getMethodName(), e.getMessage());
            }
        }
    }

    private static void parse(String input, Spotter spotter) throws SauronException, ZKNamingException {
        String[] parsed = input.split("\\s+");
        String[] args = Arrays.copyOfRange(parsed, 1, parsed.length);

        switch (parsed[0]) {
            case "spot":
                spotter.spot(args);
                break;
            case "trail":
                spotter.trail(args);
                break;
            case "ping":
                spotter.ping(args[0]);
                break;
            case "info":
                spotter.info(args[0]);
                break;
            case "clear":
                spotter.clear();
                break;
            case "help":
                usage();
                break;
            default:
                System.out.println("Unknown command");
                usage();
        }
    }

    private static void usage() {
        System.out.println("Usage:");
        System.out.println("\thelp - This help");
        System.out.println("\tinfo <name> - Shows information for camera with name <name>");
        System.out.println("\tping <name> - Check server status");
        System.out.println("\tclear - Clean server state");
        System.out.println("\tspot <type> <id | id fragment with *> - Search id/fragment for given type");
        System.out.println("\ttrail <type> <id> - Get full trace of the type with given id");
    }
}
