package pt.tecnico.sauron.silo.client;

import com.google.protobuf.Timestamp;

import io.grpc.StatusRuntimeException;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import pt.tecnico.sauron.silo.grpc.*;
import pt.tecnico.sauron.silo.lib.*;
import pt.ulisboa.tecnico.sdis.zk.ZKNaming;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;
import pt.ulisboa.tecnico.sdis.zk.ZKRecord;

import static pt.tecnico.sauron.silo.lib.SauronMessage.*;

public class SiloFrontend {
    private ZKNaming zookeeper;
    private String path;
    private SiloGrpc.SiloBlockingStub stub;
    private ManagedChannel channel;
    private pt.tecnico.sauron.silo.lib.Timestamp ts;
    private String camName = null;
    private Double camLat = null;
    private Double camLong = null;
    private Cache cache;

    /**
     * Constructor for camera clients
     *
     * @param host     The ZooKeeper host
     * @param port     The Zookeeper port
     * @param path     The path to server inside ZooKeeper
     * @param instance The instance number of server (can be null)
     * @param camName  The name of the camera
     * @param camLat   The latitude of the camera
     * @param camLong  The longitude of the camera
     * @throws ZKNamingException if operation fails when getting list of records
     * @throws SauronException   if failing to submit camera
     */
    public SiloFrontend(String host, String port, String path, String instance, String camName, Double camLat,
            Double camLong) throws ZKNamingException, SauronException {
        this(host, port, path, instance, 25);
        this.camJoin(camName, camLat, camLong);
    }

    /**
     * Constructor for frontend, creating GRPC stub and channel
     *
     * @param host       The ZooKeeper host
     * @param port       The Zookeeper port
     * @param path       The path to server inside ZooKeeper
     * @param instance   The instance number of server (can be null)
     * @param maxEntries The number of cache entries
     * @throws ZKNamingException if operation fails when getting list of records
     */
    public SiloFrontend(String host, String port, String path, String instance, int maxEntries)
            throws ZKNamingException {
        this.zookeeper = new ZKNaming(host, port);
        this.path = path;
        this.connect(instance);
        this.cache = new Cache(maxEntries);
    }

    /**
     *
     * Connect to GRPC target (and register camera if camera client)
     *
     * @param path     The path to server inside ZooKeeper
     * @param instance The instance number of server (can be null)
     * @throws ZKNamingException if operation fails when getting list of records
     */
    public void connect(String instance) throws ZKNamingException {
        String target;
        if (instance == null) {
            Collection<ZKRecord> records = this.zookeeper.listRecords(this.path);
            target = records.stream().skip((int) (Math.random() * records.size())).findFirst().get().getURI();
        } else {
            target = this.zookeeper.lookup(this.path + "/" + instance).getURI();
        }

        this.channel = ManagedChannelBuilder.forTarget(target).usePlaintext().build();

        this.stub = SiloGrpc.newBlockingStub(channel);
    }

    /**
     * Connect to another replica if channel is shutdown (and register camera if
     * camera client)
     *
     * @throws ZKNamingException if operation fails when getting list of records
     * @throws SauronException   if failing to submit camera
     */
    public void reconnectIfDown() throws ZKNamingException, SauronException {
        boolean down = false;
        try {
            this.ctrl_ping("DOWNDETECTOR");
        } catch (SauronException e) {
            down = true;
        }

        if (down || this.channel.isShutdown()) {
            this.connect(null);
            // Register camera to new server
            if (this.camName != null && this.camLat != null && this.camLong != null) {
                this.camJoin();
            }
        }
    }

    /**
     * Register camera to Silo server
     *
     * @return The camera object with given properties
     * @throws ZKNamingException if cannot connect to other replica
     * @throws SauronException   if an error occurs on the server side
     */
    public Camera camJoin() throws ZKNamingException, SauronException {
        CameraData camData;

        this.reconnectIfDown();

        try {
            camData = this.stub.camJoin(CamJoinRequest.newBuilder().setName(this.camName).setLatitude(this.camLat)
                    .setLongitude(this.camLong).build()).getCamData();
        } catch (StatusRuntimeException e) {
            throw new SauronException(e.getStatus().getDescription());
        }

        Camera camera = convertToCamera(camData);

        if (camera.getLatitude() == this.camLat && camera.getLongitude() == camLong) {
            return camera;
        } else {
            throw new SauronException(INVALID_CAMERA);
        }
    }

    /**
     * Set camera parameters and register to Silo server
     *
     * @param camName The name of the camera
     * @param camLat  The latitude of the camera
     * @param camLong The longitude of the camera
     * @return The camera object with given properties
     * @throws ZKNamingException if cannot connect to other replica
     * @throws SauronException   if an error occurs on the server side
     */
    public Camera camJoin(String camName, Double camLat, Double camLong) throws ZKNamingException, SauronException {
        this.camName = camName;
        this.camLat = camLat;
        this.camLong = camLong;

        return this.camJoin();
    }

    /**
     * Get the coordinates of the given camera
     *
     * @param name The name of the camera
     * @return The coordinates of the camera as a String
     * @throws ZKNamingException if cannot connect to other replica
     * @throws SauronException   if an error occurs on the server side
     */
    public String camInfo(String name) throws ZKNamingException, SauronException {
        CamInfoResponse response;

        this.reconnectIfDown();

        try {
            response = stub.camInfo(CamInfoRequest.newBuilder().setName(name).build());
        } catch (StatusRuntimeException e) {
            throw new SauronException(e.getStatus().getDescription());
        }
        if (response.getCamExists()) {
            return response.getCoordinates();
        } else {
            throw new SauronException(CAMERA_UNREGISTERED);
        }
    }

    /**
     * Get the most recent observation for the entities with given type and matching
     * id(s)
     *
     * @param type The type of the entity
     * @param id   The (partial) id of the entity
     * @return The list with the most recent observation for each matching entity
     * @throws ZKNamingException if cannot connect to other replica
     * @throws SauronException   if an error occurs on the server side
     */
    public List<Observation> spot(Type type, String id) throws ZKNamingException, SauronException {
        Pattern pattern = Pattern.compile(type.regex);
        Matcher matcher = pattern.matcher(id);

        List<ObservationData> observations = new ArrayList<ObservationData>();
        pt.tecnico.sauron.silo.lib.Timestamp ts;

        this.reconnectIfDown();

        try {
            // Send according to proper type
            switch (type.toString()) {
                case "person":
                    // Check if is partial identifier
                    if (matcher.matches()) {
                        TrackRequest request = TrackRequest.newBuilder().setObType(ObservableType.Person)
                                .setObId(ObservableId.newBuilder().setPerson(Long.parseLong(id)).build()).build();
                        TrackResponse response = this.stub.track(request);
                        observations.add(response.getObsData());
                        ts = convertToTimestamp(response.getTs());
                        break;
                    }

                    pattern = Pattern.compile(type.partial);
                    matcher = pattern.matcher(id);

                    if (matcher.matches()) {
                        TrackMatchRequest request = TrackMatchRequest.newBuilder().setObType(ObservableType.Person)
                                .setObId(id).build();
                        TrackMatchResponse response = this.stub.trackMatch(request);
                        observations = response.getObsDataList();
                        ts = convertToTimestamp(response.getTs());
                    } else {
                        throw new SauronException(INVALID_ID);
                    }

                    break;
                case "car":
                    // Check if is partial identifier
                    if (matcher.matches()) {
                        TrackRequest request = TrackRequest.newBuilder().setObType(ObservableType.Car)
                                .setObId(ObservableId.newBuilder().setCar(id).build()).build();
                        TrackResponse response = this.stub.track(request);
                        observations.add(response.getObsData());
                        ts = convertToTimestamp(response.getTs());
                        break;
                    }

                    pattern = Pattern.compile(type.partial);
                    matcher = pattern.matcher(id);

                    if (matcher.matches()) {
                        TrackMatchRequest request = TrackMatchRequest.newBuilder().setObType(ObservableType.Car)
                                .setObId(id).build();
                        TrackMatchResponse response = this.stub.trackMatch(request);
                        observations = response.getObsDataList();
                        ts = convertToTimestamp(response.getTs());
                    } else {
                        throw new SauronException(INVALID_ID);
                    }
                    break;
                default:
                    throw new SauronException(UNKNOWN_TYPE);
            }
        } catch (StatusRuntimeException e) {
            if (e.getStatus().getDescription().equals(ID_NOT_FOUND.message)
                    && this.cache.get("spot", type.toString() + " " + id.toString()) != null) {
                return this.cache.get("spot", type.toString() + " " + id.toString());
            }

            throw new SauronException(e.getStatus().getDescription());
        }

        List<Observation> obs = convertToObservations(observations);

        return this.cache.put("spot", type.toString() + " " + id.toString(), ts, obs);
    }

    /**
     * Get the full list of observations for the entity with given type and id
     *
     * @param type The type of the entity
     * @param id   The id of the entity
     * @return The list with all the observations from the given entity
     * @throws ZKNamingException if cannot connect to other replica
     * @throws SauronException   if an error occurs on the server side
     */
    public List<Observation> trail(Type type, String id) throws ZKNamingException, SauronException {
        TraceRequest request;
        TraceResponse response;

        this.reconnectIfDown();

        // Create request according to type
        switch (type.toString()) {
            case "person":
                checkType(Type.person, id);
                request = TraceRequest.newBuilder().setObType(ObservableType.Person)
                        .setObId(ObservableId.newBuilder().setPerson(Long.parseLong(id)).build()).build();
                break;
            case "car":
                checkType(Type.car, id);
                request = TraceRequest.newBuilder().setObType(ObservableType.Car)
                        .setObId(ObservableId.newBuilder().setCar(id).build()).build();
                break;
            default:
                throw new SauronException(UNKNOWN_TYPE);
        }

        try {
            response = this.stub.trace(request);
        } catch (StatusRuntimeException e) {
            if (e.getStatus().getDescription().equals(ID_NOT_FOUND.message)
                    && this.cache.get("trail", type.toString() + " " + id.toString()) != null) {
                return this.cache.get("trail", type.toString() + " " + id.toString());
            }
            throw new SauronException(e.getStatus().getDescription());
        }

        return this.cache.put("trail", type.toString() + " " + id.toString(), convertToTimestamp(response.getTs()),
                convertToObservations(response.getObsDataList()));
    }

    /**
     * Send observations to server
     *
     * @param observations The list of observations to send
     * @throws ZKNamingException if cannot connect to other replica
     * @throws SauronException   if an error occurs on the server side
     */
    public void report(List<Observation> observations) throws ZKNamingException, SauronException {
        List<ObservationData> resultObs = convertToObservationsData(observations);

        this.reconnectIfDown();

        try {
            this.stub.report(ReportRequest.newBuilder().addAllObs(resultObs).build());
        } catch (StatusRuntimeException e) {
            if (e.getStatus().getDescription()
                    .equals(String.format(INVALID_OBSERVATION_FOUND.message, observations.size()))) {
                if (this.stub.camInfo(CamInfoRequest.newBuilder().setName(this.camName).build()).getCamExists()) {
                    throw new SauronException(INVALID_CAMERA);
                } else {
                    this.camJoin();
                    try {
                        this.report(observations);
                    } catch (SauronException f) {
                        throw new SauronException(e.getStatus().getDescription());
                    }
                }
            } else {
                throw new SauronException(e.getStatus().getDescription());
            }
        }
    }

    /**
     * Check if server is alive
     *
     * @param input The string to echo
     * @return The echoed string
     * @throws ZKNamingException if cannot connect to other replica
     */
    public String ctrl_ping(String input) throws SauronException {
        try {
            return this.stub.ctrlPing(PingRequest.newBuilder().setText(input).build()).getText();
        } catch (StatusRuntimeException e) {
            throw new SauronException(SERVER_DISCONNECTED);
        }
    }

    /**
     * Clear server state
     *
     * @return true if all went well, false otherwise
     * @throws ZKNamingException if cannot connect to other replica
     * @throws SauronException   if cannot register camera
     */
    public boolean ctrl_clear() throws ZKNamingException, SauronException {
        this.reconnectIfDown();

        return this.stub.ctrlClear(ClearRequest.getDefaultInstance()).getText().equals("OK");
    }

    /**
     * Populate server with cameras and observations
     *
     * @return true if all went well, false otherwise
     * @throws ZKNamingException if cannot connect to other replica
     * @throws SauronException   if cannot register camera
     */
    public boolean ctrl_init() throws ZKNamingException, SauronException {
        this.reconnectIfDown();

        return this.stub.ctrlInit(InitRequest.getDefaultInstance()).getText().equals("OK");
    }

    /**
     * Convert GRPC camera to Sauron camera
     *
     * @param camData The GRPC camera
     * @return The Sauron camera
     * @throws ZKNamingException if cannot connect to other replica
     */
    public Camera convertToCamera(CameraData camData) {
        return new Camera(camData.getName(), camData.getLatitude(), camData.getLongitude());
    }

    /**
     * Convert Sauron camera to GRPC camera
     *
     * @param camera The Sauron camera
     * @return The GRPC camera
     */
    public CameraData convertToCameraData(Camera camera) {
        return CameraData.newBuilder().setLatitude(camera.getLatitude()).setLongitude(camera.getLongitude())
                .setName(camera.getName()).build();
    }

    /**
     * Convert GRPC list of observations to Sauron list of observations
     *
     * @param observations The GRPC list of observations
     * @return The Sauron list of observations
     */
    public List<Observation> convertToObservations(List<ObservationData> observations) {
        List<Observation> result = new ArrayList<Observation>();
        for (ObservationData obs : observations) {
            Observation observation = new Observation();

            observation.setCamera(convertToCamera(obs.getCamera()));

            // Create to appropriate type
            switch (obs.getObType().getNumber()) {
                case ObservableType.Person_VALUE:
                    observation.setId((Long) obs.getObId().getPerson());
                    observation.setType(Type.person);
                    break;
                case ObservableType.Car_VALUE:
                    observation.setId((String) obs.getObId().getCar());
                    observation.setType(Type.car);
                    break;
            }

            Timestamp ts = obs.getTs();
            // Convert timestamp to instant
            if (ts != null) {
                observation.setTs(Instant.ofEpochSecond(ts.getSeconds(), ts.getNanos()));
            }

            result.add(observation);
        }

        return result;
    }

    /**
     * Convert Sauron list of observations to GRPC list of observations
     *
     * @param observations The Sauron list of observations
     * @return The list of GRPC observations
     */
    public List<ObservationData> convertToObservationsData(List<Observation> observations) throws SauronException {
        List<ObservationData> result = new ArrayList<ObservationData>();
        for (Observation obs : observations) {

            ObservationData observation;
            ObservationData.Builder builder = ObservationData.newBuilder();
            builder.setCamera(convertToCameraData(obs.getCamera()));

            // Create to appropriate type
            switch (obs.getType().toString()) {
                case "person":
                    checkType(Type.person, obs.getId().toString());

                    builder.setObType(ObservableType.Person);
                    builder.setObId(ObservableId.newBuilder().setPerson((Long) obs.getId()).build());
                    break;
                case "car":
                    checkType(Type.car, obs.getId().toString());

                    builder.setObType(ObservableType.Car);
                    builder.setObId(ObservableId.newBuilder().setCar((String) obs.getId()));
                    break;
            }

            Instant instant = obs.getTs();
            // Convert instant to timestamp
            if (instant != null) {
                builder.setTs(Timestamp.newBuilder().setNanos(instant.getNano()).setSeconds(instant.getEpochSecond())
                        .build());
            }

            observation = builder.build();
            result.add(observation);
        }

        return result;
    }

    /**
     * Convert GRPC vector-timestamp to Sauron vector-timestamp
     *
     * @param timeData The GRPC vector-timestamp
     * @return The Sauron vector-timestamp
     */
    public pt.tecnico.sauron.silo.lib.Timestamp convertToTimestamp(TimestampData timeData) {
        return new pt.tecnico.sauron.silo.lib.Timestamp(timeData.getVector());
    }

    /**
     * Check if id is valid for given type
     *
     * @param type The type of the entity
     * @param id   The id of the entity
     * @throws SauronException if requirements are not fullfilled
     */
    public void checkType(Type type, String id) throws SauronException {
        Pattern pattern = Pattern.compile(type.regex);
        Matcher matcher = pattern.matcher(id);

        if (!matcher.matches()) {
            throw new SauronException(INVALID_ID);
        }
    }

    /**
     * Check if id is partial for the given type
     *
     * @param type The type of the entity
     * @param id   The partial id of the entity
     * @throws SauronException if requirements are not fullfilled
     */
    public void checkPartialType(Type type, String id) throws SauronException {
        Pattern pattern = Pattern.compile(type.partial);
        Matcher matcher = pattern.matcher(id);

        if (!matcher.matches()) {
            throw new SauronException(INVALID_ID);
        }
    }

    /**
     * Close the channel to communicate with the server.
     *
     * @apiNote Must be used before deleting SiloFrontend
     */
    public void close() {
        try {
            while (!this.channel.shutdownNow().awaitTermination(30, TimeUnit.SECONDS))
                ;
        } catch (InterruptedException e) {
            System.out.println("Interrupted Exception while closing channel");
        }
    }
}
