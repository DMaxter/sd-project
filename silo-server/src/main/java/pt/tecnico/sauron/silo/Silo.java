package pt.tecnico.sauron.silo;

import java.time.Instant;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;

import pt.tecnico.sauron.silo.lib.Observation;
import pt.tecnico.sauron.silo.lib.Camera;
import pt.tecnico.sauron.silo.lib.Type;
import pt.tecnico.sauron.silo.lib.InternalException;
import pt.tecnico.sauron.silo.domain.*;

import static pt.tecnico.sauron.silo.lib.SauronMessage.*;

public class Silo {
    private ConcurrentHashMap<Type, ConcurrentHashMap<Object, Observable>> _observations = new ConcurrentHashMap<Type, ConcurrentHashMap<Object, Observable>>();
    private ConcurrentHashMap<String, Camera> _cameras = new ConcurrentHashMap<String, Camera>();

    /**
     * Initialize data structures
     */
    public Silo() {
        for (Type t : Type.values()) {
            this._observations.put(t, new ConcurrentHashMap<Object, Observable>());
        }
    }

    /**
     * Register camera
     *
     * @param name      The name of the camera
     * @param latitude  Latitude of the camera
     * @param longitude Longitude of the camera
     * @return The camera with the given requirements
     * @throws InternalException if parameters are invalid or camera already exists
     *                           on different coordinates
     */
    public Camera camJoin(String name, double latitude, double longitude) throws InternalException {
        Camera camera;

        checkValidCameraName(name);
        checkValidCoordinates(latitude, longitude);

        if (_cameras.containsKey(name)) {
            camera = _cameras.get(name);
        } else {
            camera = new Camera(name, latitude, longitude);
            _cameras.put(name, camera);
        }
        return camera;
    }

    /**
     * Get the information of a camera
     *
     * @param name The name of the camera
     * @return The given camera coordinates
     * @throws InternalException if name is invalid or camera does not exist
     */
    public String camInfo(String name) throws InternalException {
        checkValidCameraName(name);
        Camera camera = _cameras.get(name);

        if (camera != null) {
            return "(" + camera.getLatitude() + "," + camera.getLongitude() + ")";
        } else {
            return "";
        }

    }

    /**
     * Get the most recent observation for the given entity
     *
     * @param type The type of the entity
     * @param id   The id of the entity to look for
     * @return The most recent observation for the given entity
     * @throws InternalException if no entity matches the given requirements
     */
    public Observation track(Type type, Object id) throws InternalException {
        ConcurrentHashMap<Object, Observable> obsType = _observations.get(type);
        Observable obs = obsType.get(id);
        if (obs != null) {

            // return last element of list, aka the most recent observation
            List<Observation> observations = obs.getObservations();
            observations.sort(Comparator.comparing(Observation::getTs));
            return observations.get(observations.size() - 1);
        } else {
            throw new InternalException(ID_NOT_FOUND);
        }
    }

    /**
     * Get the entities with the given partial id
     *
     * @param type The type of the entity
     * @param id   The partial id of the entity to look for
     * @return The list of the most recent observation for each matching entity
     * @throws InternalException if no entity matches the given requirements
     */
    public List<Observation> trackMatch(Type type, String id) throws InternalException {
        List<Observation> lst = new ArrayList<Observation>();
        ConcurrentHashMap<Object, Observable> obsType = _observations.get(type);
        String new_id = id.replace("*", ".*");
        for (Map.Entry<Object, Observable> entry : obsType.entrySet()) {
            Observable obs = entry.getValue();
            // check if id matches
            if (Pattern.matches(new_id, obs.getId().toString())) {
                // add the most recent observation of the observable
                List<Observation> observations = obs.getObservations();
                observations.sort(Comparator.comparing(Observation::getTs));
                lst.add(observations.get(observations.size() - 1));
            }
        }
        if (lst.size() == 0) {
            throw new InternalException(ID_NOT_FOUND);
        }
        return lst;
    }

    /**
     * Get the trace of an entity
     *
     * @param type The type of the entity
     * @param id   The id of the entity to look for
     * @return The list of observations for the given entity
     * @throws InternalException if no entity matches the given requirements
     */
    public List<Observation> trace(Type type, Object id) throws InternalException {
        if (_observations.get(type).get(id) != null) {
            return _observations.get(type).get(id).getObservations();
        } else {
            throw new InternalException(ID_NOT_FOUND);
        }
    }

    /**
     * Report observations to server
     *
     * @param obervations A list of observations
     * @throws InternalException if camera is invalid or at least observation is
     *                           invalid
     */
    public void report(List<Observation> observations, Instant instant) throws InternalException {
        Set<Observation> unrepeated = new HashSet<Observation>(observations);

        int invalid = 0;
        for (Observation obs : unrepeated) {
            Camera camera = obs.getCamera();

            checkValidCameraName(camera.getName());
            checkValidCoordinates(camera.getLatitude(), camera.getLongitude());

            Camera regCamera;
            if (!this._cameras.containsKey(camera.getName())
                    || (regCamera = this._cameras.get(camera.getName())) == null || !regCamera.equals(camera)) {
                invalid++;
                continue;
            }

            obs.setTs(instant);

            Type type = obs.getType();
            Pattern pattern = Pattern.compile(type.regex);
            Matcher matcher;

            Observable observable;
            ConcurrentHashMap<Object, Observable> regEntities = this._observations.get(type);

            switch (type.toString()) {
                case "person":
                    matcher = pattern.matcher(Long.toString((Long) obs.getId()));
                    if (!matcher.matches()) {
                        invalid++;
                        break;
                    } else if (!regEntities.containsKey(obs.getId())) {
                        regEntities.put(obs.getId(), new Person(type, (Long) obs.getId()));
                    }

                    observable = regEntities.get(obs.getId());

                    observable.addObservation(obs);

                    break;
                case "car":
                    matcher = pattern.matcher((String) obs.getId());
                    if (!matcher.matches()) {
                        invalid++;
                        break;
                    } else if (!regEntities.containsKey(obs.getId())) {
                        regEntities.put(obs.getId(), new Car(type, (String) obs.getId()));
                    }
                    observable = regEntities.get(obs.getId());

                    observable.addObservation(obs);
                    break;
                default:
                    invalid++;
            }
        }

        if (invalid != 0) {
            throw new InternalException(INVALID_OBSERVATION_FOUND, invalid);
        }
    }

    public void addObservations(List<Observation> observations, Instant instant) {
        for (Observation obs : observations) {
            obs.setTs(instant);
        }

        this.addCreatedObservations(observations);
    }

    public void addCreatedObservations(List<Observation> observations) {
        Set<Observation> unrepeated = new HashSet<Observation>(observations);

        for (Observation obs : unrepeated) {

            Type type = obs.getType();

            Observable observable;
            ConcurrentHashMap<Object, Observable> regEntities = this._observations.get(type);

            switch (type.toString()) {
                case "person":
                    if (!regEntities.containsKey(obs.getId())) {
                        regEntities.put(obs.getId(), new Person(type, (Long) obs.getId()));
                    }

                    observable = regEntities.get(obs.getId());

                    observable.addObservation(obs);

                    break;
                case "car":
                    if (!regEntities.containsKey(obs.getId())) {
                        regEntities.put(obs.getId(), new Car(type, (String) obs.getId()));
                    }
                    observable = regEntities.get(obs.getId());

                    observable.addObservation(obs);
                    break;
            }
        }
    }

    /**
     * Clear whole server state
     *
     * @return "OK" if all went well
     */
    public String clear() {
        for (Type t : Type.values()) {
            this._observations.put(t, new ConcurrentHashMap<Object, Observable>());
        }

        this._cameras = new ConcurrentHashMap<String, Camera>();

        return "OK";
    }

    /**
     * Initialize server with 12 cameras, 9 cars, 9 people and 18 observations to
     * each entity
     *
     * @return "OK" if all went well
     */
    public String init() {
        // Create 6 cameras
        for (int i = 0; i < 6; i++) {
            String name = "Camera" + i;
            Camera camera = new Camera(name, i * 10, i * -20);
            this._cameras.put(name, camera);
        }

        // Create 4 cameras for coordinates edge case testing
        this._cameras.put("Camera_-90", new Camera("Camera_-90", -90, 0));
        this._cameras.put("Camera_90", new Camera("Camera_90", 90, 0));
        this._cameras.put("Camera_-180", new Camera("Camera_-180", 0, -180));
        this._cameras.put("Camera_180", new Camera("Camera_180", 0, 180));

        // Create 2 cameras for name edge case testing

        this._cameras.put("123", new Camera("123", 1, 1));
        this._cameras.put("123456789ABCDEF", new Camera("123456789ABCDEF", 2, 2));

        // Create 9 people and 9 cars with 18 observations each
        for (int i = 0; i < 18; i++) {
            Type type;
            Object id;
            Observable observable;
            if (i % 2 == 0) {
                type = Type.valueOf("person");
                id = Long.parseLong(i + "1");
                observable = new Person(type, Long.parseLong(id.toString()));
            } else {
                type = Type.valueOf("car");
                id = "AA00" + (i < 10 ? "0" + i : i);
                observable = new Car(type, id.toString());
            }

            ConcurrentHashMap<Object, Observable> obsType = this._observations.get(type);
            obsType.put(id, observable);
            for (int j = 0; j < 18; j++) {
                String name = "Camera" + j % 6;
                Observation observation = new Observation(type, id, this._cameras.get(name));
                List<Observation> observations = new ArrayList<Observation>();
                observation.setTs(Instant.now());
                observations.add(observation);
                try {
                    report(observations, Instant.now());
                } catch (InternalException e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        return "OK";
    }

    /**
     * Check if name is at least 3 characters and not bigger than 15
     *
     * @param name name of the camera
     * @throws InternalException if requirements are not fullfilled
     */
    public void checkValidCameraName(String name) throws InternalException {
        if (3 > name.length() || name.length() > 15)
            throw new InternalException(INVALID_CAMERA_NAME);
    }

    /**
     * Check if coordinates are valid
     *
     * @param lat The latitude of the camera
     * @param lon The longitude of the camera
     * @throws InternalException if coordinates are invalid
     */
    public void checkValidCoordinates(double lat, double lon) throws InternalException {
        if (-90 > lat || 90 < lat || -180 > lon || 180 < lon)
            throw new InternalException(INVALID_COORDINATES);
    }

    public List<Observation> getObservationsList() {
        List<Observation> observations = new ArrayList<>();
        for (Map<Object, Observable> type : this._observations.values()) {
            for (Observable observable : type.values()) {
                observations.addAll(observable.getObservations());
            }
        }
        return observations;
    }

}
