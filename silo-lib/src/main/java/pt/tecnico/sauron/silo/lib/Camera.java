package pt.tecnico.sauron.silo.lib;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Camera {
    private String name;
    private double latitude;
    private double longitude;

    public Camera(String name, double latitude, double longitude) {
        this.setName(name);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public double getLatitude(){
        return latitude;
    }

    public void setLatitude(double latitude){
        this.latitude = latitude;
    }

    public double getLongitude(){
        return longitude;
    }

    public void setLongitude(double longitude){
        this.longitude = longitude;
    }

    public boolean equals(Object o){
        if (!(o instanceof Camera)){
            return false;
        }

        Camera c = (Camera) o;

        return c.getName().equals(this.name)
            && c.getLatitude() == this.latitude
            && c.getLongitude() == this.longitude;
    }

    @Override
    public String toString(){
        DecimalFormatSymbols ns = new DecimalFormatSymbols();
        ns.setDecimalSeparator('.');

        DecimalFormat df = new DecimalFormat("0.000000", ns);
        return name + "," + df.format(latitude) + "," + df.format(longitude);
    }
}
