package pt.tecnico.sauron.silo.lib;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class Observation implements Comparable<Observation> {
    private Type type;
    private Object id;
    private Instant ts;
	private Camera camera;
    public Observation(){}

    public Observation(Type type, Object id, Instant ts) {
        this.setType(type);
        this.setId(id);
        this.setTs(ts);
    }

	public Observation(Type type, Object id, Camera camera) {
        this.setType(type);
        this.setId(id);
        this.setCamera(camera);
    }

	public Instant getTs() {
		return ts;
	}

	public void setTs(Instant ts) {
		this.ts = ts;
	}

	public Object getId() {
		return this.id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	public Type getType() {
		return this.type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Camera getCamera(){
		return camera;
	}

	public void setCamera(Camera camera){
		this.camera = camera;
	}

    public String toString(){
        return this.type + "," +
            this.id + "," +
            LocalDateTime.ofInstant(this.ts, ZoneOffset.UTC) + "," +
            this.camera;
    }

    public int compareTo(Observation o){
        Object id = o.getId();

        if(id instanceof String && this.id instanceof String){
            return ((String) this.id).compareTo((String) id);
        }else if(id instanceof Long && this.id instanceof Long){
            return ((Long) this.id).compareTo((Long) id);
        }else{
            return 0;
        }
    }
}
