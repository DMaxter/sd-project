package pt.tecnico.sauron.silo.lib;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.lang.Math.*;


public class Timestamp {

    private Map<Integer,Integer> timestamp ;

    /**
     * Initialize data structures
     */
    public Timestamp(){
        this.timestamp = new ConcurrentHashMap<Integer,Integer>();
    }

    public Timestamp(Map<Integer,Integer> vector) {
        this.timestamp = new ConcurrentHashMap<Integer,Integer>(vector);
    }

    /**
     * Increments update id for desired replica
     *
     * @param replica replica's numeric id
     *
     */
    public void incrementUpdate(int replica) {
        if(!this.timestamp.containsKey(replica)){
            this.timestamp.put(replica,1);
        } else {
            this.timestamp.put(replica, this.timestamp.get(replica) + 1);
        }
    }

    /**
     * Merges two timestamps' update for given replica
     *
     * @param other Timestamp to be merged with
     * @param replica Replica to merge timestamp update
     */
    public void merge(Timestamp other, int replica) {
        this.timestamp.put(replica, Math.max(this.get(replica), other.get(replica)));
    }

    /**
     * Determines if timestamp happens before another timestamp
     *
     * @param other timestamp to be compared to
     * @return true if happens before, false if not
     *
     */
    public boolean happensBefore(Timestamp other) {
        if(other == null){
            return false;
        }

        for(Integer key : other.getTimestamp().keySet()){
            if(!this.happensBefore(other, key)){
                return false;
            }
        }
        return true;
    }

    /**
     * Determines if timestamp happens before another timestamp
     *
     * @param other Timestamp to be compared to
     * @param replica Replica number to compare
     * @return true if happens before, false if not
     *
     */
    public boolean happensBefore(Timestamp other, int replica) {
        return this.get(replica) < other.get(replica);
    }

    /**
     * Getter for update on a particular replica
     *
     * @param replica replica's numeric id
     * @return update number
     *
     */
    public int get(int replica){
        if(this.timestamp.get(replica) == null) {
            this.timestamp.put(replica, 0);
        }

        return this.timestamp.get(replica);
    }

    /**
     * Getter for timestamp vector
     *
     * @return timestamp vector
     *
     */
    public Map<Integer,Integer> getTimestamp() {
        return this.timestamp;
    }

    /**
     * Setter for timestamp vector
     *
     * @param vector timestamp vector
     *
     */
    public void setTimestamp(Map<Integer,Integer> vector) {
        this.timestamp = new ConcurrentHashMap<Integer,Integer>(vector);
    }

    /**
     * Getter for the number of replicas serving server
     *
     * @return number of replicas in server
     *
     */
    public Integer getNumberOfReplicas() {
        return this.timestamp.size();
    }

    /**
     * Create copy of current timestamp
     *
     * @return The copy of current timestamp
     */
    public Timestamp copy() {
        return new Timestamp(this.timestamp);
    }

    @Override
    public boolean equals(Object o){
        if (!(o instanceof Timestamp)){
            return false;
        }

        Timestamp t = (Timestamp) o;

        if (this.timestamp.size() != t.getTimestamp().size()) {
            return false;
        }

        return this.timestamp.equals(t.getTimestamp());
    }

    @Override
    public int hashCode() {
        return this.timestamp.hashCode();
    }

    @Override
    public String toString(){
        return this.timestamp.keySet().stream().map(key -> key + ":" + this.timestamp.get(key)).collect(Collectors.joining(", ", "{", "}"));
    }
}
