package pt.tecnico.sauron.eye;

import pt.tecnico.sauron.silo.lib.SauronException;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;
import static pt.tecnico.sauron.silo.lib.SauronMessage.*;

import java.io.*;

public class EyeApp {

    public static void main(String[] args) throws IOException, ZKNamingException {
        System.out.println(EyeApp.class.getSimpleName());

        // check arguments
        try {
            checkArguments(args);
        } catch (SauronException e) {
            System.out.printf("Error: %s%n", e.getMessage());
            return;
        }

        // save arguments
        final String zooHost = args[0];
        final String zooPort = args[1];
        final String path = args[2];
        final String cameraName = args[3];
        final Double cameraLatitude = Double.parseDouble(args[4]);
        final Double cameraLongitude = Double.parseDouble(args[5]);
        String instance = null;

        if (args.length == 7) {
            instance = args[6];
        }

        // register camera
        Eye eye;
        try {
            eye = new Eye(zooHost, zooPort, path, instance, cameraName, cameraLatitude, cameraLongitude);
        } catch (SauronException e) {
            System.out.printf("Error: %s%n", e.getMessage());
            return;
        }

        // Shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            eye.close();
        }));

        // messages in terminal to confirm input acceptance
        System.out.printf("Registering camera: %s (%f,%f)%n", cameraName, cameraLatitude, cameraLongitude);

        String obs = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        // reads input
        while (obs != null) {
            System.out.print("> ");
            obs = reader.readLine();

            try {
                parse(obs, eye);
            } catch (InterruptedException e) {
                System.out.printf("%s: %s%n", e.getStackTrace()[0].getMethodName(), e.getMessage());
            } catch (SauronException e) {
                System.out.printf("Error: %s%n", e.getMessage());
                if (e.getMessage().equals(SERVER_DISCONNECTED)) {
                    return;
                }
            }
        }
    }

    /**
     * Checks if correct amount of arguments as been provided
     *
     * @param args Arguments array
     * @throws SauronException if number of arguments is incorrect or if inserted
     *                         camera name/coordinates are invalid
     */
    public static void checkArguments(String[] args) throws SauronException {
        if (args.length < 6 || args.length > 7)
            throw new SauronException(WRONG_USAGE);
        else {
            checkValidCameraName(args[3]);
            checkCoordinates(args[4], args[5]);
        }

    }

    /**
     * Receives commands form terminal and parses accordingly Saves observations
     * inserted Sends to server after empty line is inserted
     *
     * @param line input inserted before end of line (\n)
     * @throws SauronException      if line does not have valid format for known
     *                              command
     * @throws InterruptedException if thread receives a termination request
     */
    public static void parse(String line, Eye eye) throws ZKNamingException, SauronException, InterruptedException {
        if (line != null && line.trim().isEmpty() || line == null) { // checks if empty line or end of file is found
            if (line == null)
                System.out.printf("%n");
            eye.sendToServer(); // sends all observations currently in buffer
            return;
        } else if (line.startsWith("#")) { // comment
            return;
        } else {
            String[] args = line.split(",");
            if (args.length != 2) {
                throw new SauronException(WRONG_USAGE);
            } else if (args[0].trim().equals("zzz")) { // sleep command
                int time = Integer.parseInt(args[1].trim());
                eye.sleep(time);
            } else {
                eye.parseData(args[0].trim(), args[1].trim()); // checks if data is valid to send
            }
        }
    }

    /**
     * Checks if camera has a valid name (3-15 chars)
     *
     * @param name camera name
     * @throws SauronException if camera name does not have required number of chars
     */
    public static void checkValidCameraName(String name) throws SauronException {
        if (3 > name.length() || name.length() > 15)
            throw new SauronException(INVALID_CAMERA_NAME);
    }

    /**
     * Checks if camera has valid coordinates (-90 < lat < 90 and -180 < long < 180)
     *
     * @param latitude  camera latitude
     * @param longitude camera longitude
     * @throws SauronException if camera does not have valid coordinates
     */
    public static void checkCoordinates(String latitude, String longitude) throws SauronException {
        double lat = Double.parseDouble(latitude);
        double lon = Double.parseDouble(longitude);
        if (-90 > lat || 90 < lat || -180 > lon || 180 < lon)
            throw new SauronException(INVALID_COORDINATES);
    }

}
