package pt.tecnico.sauron.eye;

import io.grpc.StatusRuntimeException;

import pt.tecnico.sauron.silo.lib.SauronException;
import pt.tecnico.sauron.silo.lib.Observation;
import pt.tecnico.sauron.silo.client.SiloFrontend;
import pt.tecnico.sauron.silo.lib.Camera;
import pt.tecnico.sauron.silo.lib.Type;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;
import static pt.tecnico.sauron.silo.lib.SauronMessage.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Eye {

    private LinkedList<String> obsBuffer = new LinkedList<String>(); // saves all observations registered before sending
    private SiloFrontend frontend;
    private Camera camera; // camera that sends observations

    /**
     * Initialize data structures and register camera
     *
     * @param name      The name of the camera
     * @param latitude  Latitude of the camera
     * @param longitude Longitude of the camera
     * @throws ZKNamingException if failing to get name from ZooKeeper
     * @throws SauronException if failing to submit camera
     */
    Eye(String host, String port, String path, String instance, String name, double latitude, double longitude) throws ZKNamingException, SauronException {
        this.frontend = new SiloFrontend(host, port, path, instance, name, latitude, longitude);
        this.camera = new Camera(name, latitude, longitude);
    }

    /**
     * Sends observations in buffer to server
     *
     * @throws ZKNamingException if cannot connect to other replica
     * @throws SauronException if type is unknown
     */
    public void sendToServer() throws ZKNamingException, SauronException {
        LinkedList<Observation> observations = new LinkedList<Observation>();
        if(obsBuffer.size() != 0) {
            System.out.println("Sending to camera: " + this.camera.getName());
        }
        else {
            System.out.println("No observations to send");
        }
        while(obsBuffer.size() != 0){
            String[] obs = obsBuffer.pop().split(",");
            Type type;
            Object id;

            try { //check if valid type
                type = Type.valueOf(obs[0].trim());
            } catch (NullPointerException | IllegalArgumentException e) {
                throw new SauronException(UNKNOWN_TYPE);
            }
            switch (type.toString()){
                case "person":
                    id = Long.parseLong(obs[1].trim());
                    break;
                case "car":
                    id = obs[1].trim();
                    break;
                default:
		            throw new SauronException(UNKNOWN_TYPE);
            }

            // add observation in buffer to observation list to be sent
            Observation observation = new Observation(type, id, this.camera);
            observations.add(observation);

            System.out.println("- " + observation.getType() + ", " + observation.getId());
        }
        if(observations.size() > 0){
            try {
                frontend.report(observations);
            } catch (StatusRuntimeException e){
		        throw new SauronException(SERVER_DISCONNECTED);
		    }
        }
    }

    /**
     * Pauses program execution for x milliseconds
     *
     * @param milliseconds     Amount of time (in milliseconds) to pause program execution
     * @throws InterruptedException     if thread receives a termination request
     */
    public void sleep(int milliseconds) throws InterruptedException{
        Thread.sleep(milliseconds);
    }

    /**
     * Recognizes commands and parses accordingly
     *
     * @param type     Type of entity to observe
     * @param id       Entity's identification
     * @throws SauronException     if type is unknown or id is invalid for
     *                             that type
     */
    public void parseData(String type, String id) throws SauronException {
        Type _type;

        try{ //check if valid type
            _type = Type.valueOf(type);
        } catch (NullPointerException | IllegalArgumentException e) {
            throw new SauronException(UNKNOWN_TYPE);
        }

        // check if valid id
        Pattern pattern = Pattern.compile(_type.regex);
        Matcher matcher = pattern.matcher(id);

        if (!matcher.matches()) {
            throw new SauronException(INVALID_ID);
        }
        obsBuffer.add(type + "," + id);
    }

    /**
     *
     * @return camera that is sending observations to server
     */
    public Camera getCamera(){
        return this.camera;
    }

    /**
     * Closes channel connecting to frontend
     */
    public void close() {
        frontend.close();
    }
}
