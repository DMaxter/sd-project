package pt.tecnico.sauron.silo;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.grpc.BindableService;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import pt.ulisboa.tecnico.sdis.zk.ZKNaming;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;

public class SiloServerApp {
    public static void main(String[] args) throws IOException, InterruptedException, ZKNamingException {
        System.out.println("Silo Server Initializing...");

        // check arguments
        if (args.length < 7) {
            System.err.println("Argument(s) missing!");
            System.err.printf("Usage: java %s port%n", SiloServerApp.class.getName());
            return;
        } else if (args.length != 7) {
            System.out.println("Error: invalid usage");
        }

        System.out.println("Running on port " + args[3]);

        final String zooHost = args[0];
        final String zooPort = args[1];
        final String serverHost = args[2];
        final String serverPort = args[3];
        final String serverPath = args[4];
        final int instance = Integer.parseInt(args[5]);
        final Long timer = Long.parseLong(args[6]);

        try {
            final ZKNaming zkNaming = new ZKNaming(zooHost, zooPort);
            zkNaming.rebind(serverPath, serverHost, serverPort);

            final SiloServerImpl impl = new SiloServerImpl(instance, zooHost, zooPort, "/grpc/sauron/silo");

            // Create a new server to listen on port
            Server server = ServerBuilder.forPort(Integer.parseInt(serverPort))
                    .maxInboundMessageSize(1024 * 1024 * 1024).addService((BindableService) impl).build();

            // Start the server
            server.start();

            ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
            scheduler.scheduleWithFixedDelay(new Thread(() -> {
                try {
                    impl.gossip();
                } catch (ZKNamingException e) {
                    System.out.println("Could not gossip with replica");
                }
            }), 0, timer, TimeUnit.MILLISECONDS);

            System.out.println("Gossip every " + timer / 1000 + " seconds");

            // Server threads are running in the background.
            System.out.printf("Replica %d started%n", instance);

            // Shutdown hook to prevent worse crashes
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                scheduler.shutdownNow();
                try {
                    zkNaming.unbind(serverPath, serverHost, serverPort);
                } catch (ZKNamingException e) {
                }
            }));

            // Thread to kill server
            new Thread(() -> {
                System.out.println("Press <ENTER> to shutdown");
                new Scanner(System.in).nextLine();

                scheduler.shutdown();
                try {
                    if (!scheduler.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                        scheduler.shutdownNow();
                    }
                } catch (InterruptedException e) {
                    scheduler.shutdownNow();
                }
                server.shutdown();
            }).start();

            // Do not exit the main thread. Wait until server is terminated.
            server.awaitTermination();
        } catch (ZKNamingException e) {

        }
    }

}
