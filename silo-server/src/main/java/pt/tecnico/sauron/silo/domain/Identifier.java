package pt.tecnico.sauron.silo.domain;

public class Identifier {

    private Object _id;

    public Object getId() {
        return _id;
    }

    public void setId(Object id) {
        _id = id;
    }
}