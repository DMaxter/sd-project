package pt.tecnico.sauron.silo.client;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import pt.tecnico.sauron.silo.lib.SauronException;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;

import java.io.IOException;
import java.util.Properties;

public class BaseIT {

    protected static SiloFrontend frontend;
    private static final String TEST_PROP_FILE = "/test.properties";
    protected static Properties testProps;

    @BeforeAll
    public static void oneTimeSetup() throws IOException, ZKNamingException, SauronException {
        testProps = new Properties();

        try {
            testProps.load(BaseIT.class.getResourceAsStream(TEST_PROP_FILE));
            System.out.println("Test properties:");
            System.out.println(testProps);
        } catch (IOException e) {
            final String msg = String.format("Could not load properties file {}", TEST_PROP_FILE);
            System.out.println(msg);
            throw e;
        }

        frontend = new SiloFrontend(testProps.getProperty("zoo.host"), testProps.getProperty("zoo.port"),
                testProps.getProperty("path"), "1", 25);
        frontend.ctrl_clear();
        frontend.ctrl_init();
    }

    @AfterAll
    public static void cleanup() {
        frontend.close();
    }
}
