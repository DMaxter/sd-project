package pt.tecnico.sauron.silo.client;

import java.util.List;
import java.util.ArrayList;

import org.junit.jupiter.api.*;
import pt.tecnico.sauron.silo.lib.Observation;
import pt.tecnico.sauron.silo.lib.Camera;
import pt.tecnico.sauron.silo.lib.Type;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;
import pt.tecnico.sauron.silo.lib.SauronException;
import static pt.tecnico.sauron.silo.lib.SauronMessage.*;

import java.time.Instant;
import java.time.Duration;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ReportIT extends BaseIT {
    private List<Observation> observations = new ArrayList<Observation>();
    private Camera camera;
    private Camera default_camera = new Camera("Camera0", 0, 0);
    private Observation observation;

    // initialization and clean-up for each test

    @BeforeEach
    public void setUp() throws ZKNamingException, SauronException {
        frontend.ctrl_init();

    }

    @AfterEach
    public void tearDown() throws ZKNamingException, SauronException {
        frontend.ctrl_clear();
    }

    // tests

    // Test report a person successfully
    @Test
    public void reportPersonSuccessTest() throws ZKNamingException, SauronException {
        long id = 101010L;
        observation = new Observation(Type.person, id, default_camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        frontend.report(observations);

        List<Observation> result = frontend.trail(Type.person, Long.toString(id));
        assertEquals(observations.get(0).getType(), result.get(0).getType());
        assertEquals(observations.get(0).getId(), result.get(0).getId());
        assertEquals(
                Duration.between(observations.get(0).getTs(), result.get(0).getTs()).getSeconds() < 60 ? true : false,
                true);
        assertEquals(observations.get(0).getCamera(), result.get(0).getCamera());
    }

    // Test report a car successfully
    @Test
    public void reportCarSuccessTest() throws ZKNamingException, SauronException {
        String id = "AB0001";
        observation = new Observation(Type.car, id, default_camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        frontend.report(observations);

        List<Observation> result = frontend.trail(Type.car, id);
        assertEquals(observations.get(0).getType(), result.get(0).getType());
        assertEquals(observations.get(0).getId(), result.get(0).getId());
        assertEquals(
                Duration.between(observations.get(0).getTs(), result.get(0).getTs()).getSeconds() < 60 ? true : false,
                true);
        assertEquals(observations.get(0).getCamera(), result.get(0).getCamera());
    }

    // Camera coordinates edge cases
    // Latitude

    // Test invalid latitude on lower edge
    @Test
    public void reportInvalidCameraCoordinatesWrongLatitudeTest1() throws ZKNamingException, SauronException {
        camera = new Camera("Camera", -90.1, 0);
        observation = new Observation(Type.person, 101010L, camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_COORDINATES.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test invalid latitude on upper edge
    @Test
    public void reportInvalidCameraCoordinatesWrongLatitudeTest2() throws ZKNamingException, SauronException {
        camera = new Camera("Camera", 90.1, 0);
        observation = new Observation(Type.person, 101010L, camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_COORDINATES.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test minimum valid latitude (lower edge)
    @Test
    public void reportValidCameraCoordinatesLatitudeTest1() throws ZKNamingException, SauronException {
        long id = 101010L;
        camera = new Camera("Camera_-90", -90, 0);
        observation = new Observation(Type.person, id, camera);
        observation.setTs(Instant.now());
        observations.add(observation);
        frontend.report(observations);

        List<Observation> result = frontend.trail(Type.person, Long.toString(id));
        assertEquals(observations.get(0).getType(), result.get(0).getType());
        assertEquals(observations.get(0).getId(), result.get(0).getId());
        assertEquals(
                Duration.between(observations.get(0).getTs(), result.get(0).getTs()).getSeconds() < 60 ? true : false,
                true);
        assertEquals(observations.get(0).getCamera(), result.get(0).getCamera());
    }

    // Test maximum valid latitude (upper edge)
    @Test
    public void reportValidCameraCoordinatesLatitudeTest2() throws ZKNamingException, SauronException {
        long id = 101010L;
        camera = new Camera("Camera_90", 90, 0);
        observation = new Observation(Type.person, id, camera);
        observation.setTs(Instant.now());
        observations.add(observation);
        frontend.report(observations);

        List<Observation> result = frontend.trail(Type.person, Long.toString(id));
        assertEquals(observations.get(0).getType(), result.get(0).getType());
        assertEquals(observations.get(0).getId(), result.get(0).getId());
        assertEquals(
                Duration.between(observations.get(0).getTs(), result.get(0).getTs()).getSeconds() < 60 ? true : false,
                true);
        assertEquals(observations.get(0).getCamera(), result.get(0).getCamera());
    }

    // Longitude

    // Test invalid longitude on lower edge
    @Test
    public void reportInvalidCameraCoordinatesWrongLongitudeTest1() throws ZKNamingException, SauronException {
        camera = new Camera("Camera", 0, -180.1);
        observation = new Observation(Type.person, 101010L, camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_COORDINATES.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test invalid longitude on upper edge
    @Test
    public void reportInvalidCameraCoordinatesWrongLongitudeTest2() throws ZKNamingException, SauronException {
        camera = new Camera("Camera", 0, 180.1);
        observation = new Observation(Type.person, 101010L, camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_COORDINATES.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test minimum valid latitude (lower edge)
    @Test
    public void reportValidCameraCoordinatesLongitudeTest1() throws ZKNamingException, SauronException {
        long id = 101010L;
        camera = new Camera("Camera_-180", 0, -180);
        observation = new Observation(Type.person, id, camera);
        observation.setTs(Instant.now());
        observations.add(observation);
        frontend.report(observations);

        List<Observation> result = frontend.trail(Type.person, Long.toString(id));
        assertEquals(observations.get(0).getType(), result.get(0).getType());
        assertEquals(observations.get(0).getId(), result.get(0).getId());
        assertEquals(
                Duration.between(observations.get(0).getTs(), result.get(0).getTs()).getSeconds() < 60 ? true : false,
                true);
        assertEquals(observations.get(0).getCamera(), result.get(0).getCamera());
    }

    // Test maximum valid latitude (upper edge)
    @Test
    public void reportValidCameraCoordinatesLongitudeTest2() throws ZKNamingException, SauronException {
        long id = 101010L;
        camera = new Camera("Camera_180", 0, 180);
        observation = new Observation(Type.person, id, camera);
        observation.setTs(Instant.now());
        observations.add(observation);
        frontend.report(observations);

        List<Observation> result = frontend.trail(Type.person, Long.toString(id));
        assertEquals(observations.get(0).getType(), result.get(0).getType());
        assertEquals(observations.get(0).getId(), result.get(0).getId());
        assertEquals(
                Duration.between(observations.get(0).getTs(), result.get(0).getTs()).getSeconds() < 60 ? true : false,
                true);
        assertEquals(observations.get(0).getCamera(), result.get(0).getCamera());
    }

    // Test report person with a camera with same name as existing one but different
    // coordinates
    @Test
    public void reportInvalidCameraTest() throws ZKNamingException, SauronException {
        // Camera with name "Camera0" already exists with coordinates (0,0)
        camera = new Camera("Camera0", 10, 10);
        observation = new Observation(Type.person, 101010L, camera);
        observation.setTs(Instant.now());
        observations.add(observation);

    }

    // Camera name edge test cases

    // Test camera name with insuficient characters (<3 chars)
    @Test
    public void reportInvalidCameraNameTest1() throws ZKNamingException, SauronException {
        camera = new Camera("12", 0, 0);
        observation = new Observation(Type.person, 101010L, camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_CAMERA_NAME.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test camera name with too many characters (15< chars)
    @Test
    public void reportInvalidCameraNameTest2() throws ZKNamingException, SauronException {
        camera = new Camera("123456789ABCDEFG", 0, 0);
        observation = new Observation(Type.person, 101010L, camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_CAMERA_NAME.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test camera name with minimum number of characters
    @Test
    public void reportValidCameraNameTest1() throws ZKNamingException, SauronException {
        long id = 101010L;
        camera = new Camera("123", 1, 1);
        observation = new Observation(Type.person, id, camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        frontend.report(observations);

        List<Observation> result = frontend.trail(Type.person, Long.toString(id));
        assertEquals(observations.get(0).getType(), result.get(0).getType());
        assertEquals(observations.get(0).getId(), result.get(0).getId());
        assertEquals(
                Duration.between(observations.get(0).getTs(), result.get(0).getTs()).getSeconds() < 60 ? true : false,
                true);
        assertEquals(observations.get(0).getCamera(), result.get(0).getCamera());
    }

    // Test camera name with maximum number of characters
    @Test
    public void reportValidCameraNameTest2() throws ZKNamingException, SauronException {
        long id = 101010L;
        camera = new Camera("123456789ABCDEF", 2, 2);
        observation = new Observation(Type.person, id, camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        frontend.report(observations);

        List<Observation> result = frontend.trail(Type.person, Long.toString(id));
        assertEquals(observations.get(0).getType(), result.get(0).getType());
        assertEquals(observations.get(0).getId(), result.get(0).getId());
        assertEquals(
                Duration.between(observations.get(0).getTs(), result.get(0).getTs()).getSeconds() < 60 ? true : false,
                true);
        assertEquals(observations.get(0).getCamera(), result.get(0).getCamera());
    }

    // Test report person with unregistered camera
    @Test
    public void reportUnregisteredCameraTest() throws ZKNamingException, SauronException {
        camera = new Camera("cam", 0, 0);
        observation = new Observation(Type.person, 101010L, camera);
        observation.setTs(Instant.now());
        observations.add(observation);
        // Nothing happens, it tries to re-register
    }

    // Test report person with invalid id (string)
    @Test
    public void reportInvalidPersonIdTest() throws ZKNamingException, SauronException {
        String id = "wrong";
        observation = new Observation(Type.person, id, default_camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test report car with invalid id (long)
    @Test
    public void reportInvalidCarIdTest1() throws ZKNamingException, SauronException {
        long id = 123L;
        observation = new Observation(Type.car, id, default_camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test report car with invalid id (incorrect format)
    @Test
    public void reportInvalidCarIdTest2() throws ZKNamingException, SauronException {
        String id = "000EE0";
        observation = new Observation(Type.person, id, default_camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test report car with invalid id (insufficent chars)
    @Test
    public void reportInvalidCarIdTest3() throws ZKNamingException, SauronException {
        String id = "00EE0";
        observation = new Observation(Type.person, id, default_camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test report car with invalid id (only integers)
    @Test
    public void reportInvalidCarIdTest4() throws ZKNamingException, SauronException {
        String id = "000000";
        observation = new Observation(Type.person, id, default_camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test report car with invalid id (only letters)
    @Test
    public void reportInvalidCarIdTest5() throws ZKNamingException, SauronException {
        String id = "EEEEEE";
        observation = new Observation(Type.car, id, default_camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

    // Test report car with invalid id (lower case letters)
    @Test
    public void reportInvalidCarIdTest6() throws ZKNamingException, SauronException {
        String id = "00ee00";
        observation = new Observation(Type.car, id, default_camera);
        observation.setTs(Instant.now());
        observations.add(observation);

        assertEquals(INVALID_ID.message,
                assertThrows(SauronException.class, () -> frontend.report(observations)).getMessage());
    }

}
