package pt.tecnico.sauron.silo.domain;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import pt.tecnico.sauron.silo.lib.Observation;
import pt.tecnico.sauron.silo.lib.Type;

public abstract class Observable {
    private Type _type;
    private Object _id;
    private List<Observation> _observations = new CopyOnWriteArrayList<Observation>();

    public Observable(Type type, Object id) {
        _type = type;
        _id = id;
    }

    public Object getId() {
        return _id;
    }

    public void setId(Object _id) {
        this._id = _id;
    }

    public List<Observation> getObservations() {
        return _observations;
    }

    public void setObservations(List<Observation> _observations) {
        this._observations = _observations;
    }

	public Type getType() {
        return _type;
    }

    public void setType(Type type) {
        _type = type;
    }

    public void addObservation(Observation observation){
        _observations.add(observation);
    }

    public String toString(){
        return _type.toString() + "," + _id.toString();
    }
}
