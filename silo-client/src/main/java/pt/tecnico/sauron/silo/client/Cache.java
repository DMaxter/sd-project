package pt.tecnico.sauron.silo.client;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pt.tecnico.sauron.silo.lib.Observation;
import pt.tecnico.sauron.silo.lib.SauronException;
import pt.tecnico.sauron.silo.lib.Timestamp;

public class Cache {
    private Integer cacheSize;
    private LinkedHashMap<String, CacheEntry> cache;

    public class CacheEntry {
        private Timestamp ts;
        private List<Observation> obs;

        CacheEntry(Timestamp ts, List<Observation> obs) {
            this.ts = ts;
            this.obs = obs;
        }

        public Timestamp getTs() {
            return this.ts;
        }

        public void setTs(Timestamp ts) {
            this.ts = ts;
        }

        public List<Observation> getData() {
            return this.obs;
        }

        public void setData(List<Observation> obs) {
            this.obs = obs;
        }
    }

    public Cache(int cacheSize) {
        this.setCacheSize(cacheSize);
        this.cache = new LinkedHashMap<String, CacheEntry>(cacheSize, 0.75f, true) {
            protected boolean removeEldestEntry(Map.Entry eldest) {
                return size() > cacheSize;
            }
        };
    }

    public Integer getCacheSize() {
		return cacheSize;
	}

	public void setCacheSize(Integer cacheSize) {
		this.cacheSize = cacheSize;
	}

    public int size() {
        return this.cache.size();
    }

	public List<Observation> get(String requestType, String requestParam) {
        String key = requestType + " " + requestParam;

        if(this.cache.get(key) == null){
            return null;
        }
        return this.cache.get(key).getData();
    }

    public List<Observation> put(String requestType, String requestParam, Timestamp ts, List<Observation> obs) throws SauronException {
        String key = requestType + " " + requestParam;

        CacheEntry entry = this.cache.get(key);
        if (entry != null && !entry.getTs().happensBefore(ts)) {
            return entry.getData();
        }

        this.cache.put(key, new CacheEntry(ts, obs));
        return obs;
    }
}
