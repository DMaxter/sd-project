package pt.tecnico.sauron.silo.lib;

public enum SauronMessage {
    CAMERA_UNREGISTERED("Unregistered camera"),
    EMPTY_INPUT("Input cannot be empty"),
    ID_NOT_FOUND("Couldn't find id"),
    INCOMPATIBLE_TIMESTAMPS("Timestamps must have same size"),
    INVALID_CAMERA("Camera already exists with different coordinates"),
    INVALID_CAMERA_NAME("Camera name must have between 3-15 characters"),
    INVALID_COORDINATES("Invalid coordinates"),
    INVALID_ID("Invalid id for given type"),
    INVALID_OBSERVATION_FOUND("Found %d invalid observation(s)"),
    OPERATION_NOT_COMPLETED("Operation could not be completed"),
    SERVER_DISCONNECTED("Cannot connect to server"),
    UNKNOWN_TYPE("Unknown type"),
    WRONG_USAGE("Wrong usage"),
    RECORDS_NOT_TRANSMITTED("Record List wasn't correctly transmitted"),
    RECORD_NOT_TRANSMITTED("A Record wasn't correctly transmitted"),
    INSTANT_NOT_TRANSMITTED("Instant wasn't correctly transmitted"),
    TIMESTAMP_NOT_TRANSMITTED("Timestamp wasn't correctly transmitted");


    public final String message;
    private SauronMessage(String message){
        this.message = message;
    }
};
