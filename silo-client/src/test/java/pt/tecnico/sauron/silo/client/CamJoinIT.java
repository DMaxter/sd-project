package pt.tecnico.sauron.silo.client;

import org.junit.jupiter.api.*;
import pt.tecnico.sauron.silo.lib.Camera;
import pt.tecnico.sauron.silo.lib.SauronException;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static pt.tecnico.sauron.silo.lib.SauronMessage.INVALID_CAMERA_NAME;
import static pt.tecnico.sauron.silo.lib.SauronMessage.INVALID_CAMERA;
import static pt.tecnico.sauron.silo.lib.SauronMessage.INVALID_COORDINATES;

public class CamJoinIT extends BaseIT {

    Camera camName3 = new Camera("abc", 10.000, 10.000);
	Camera camName15 = new Camera("123456789abcdef", 10.000, 10.000);
	Camera camLat90 = new Camera("Latitude90", 90.0, 0.0);
	Camera camLatNeg90 = new Camera("LatitudeNeg90", -90.0, 0.0);
	Camera camLong180 = new Camera("Longitude180", 0.0, 180.0);
	Camera camLongNeg180 = new Camera("LongitudeNeg180", 0.0, -180.0);
	Camera duplicateCam = new Camera("Duplicate", 10.000, 10.000);
	
	//Test name
	@Test
    public void CamJoinName2() throws ZKNamingException, SauronException {
        assertEquals(INVALID_CAMERA_NAME.message,
                assertThrows(SauronException.class, () -> frontend.camJoin("12", 10.000, 10.000)).getMessage());
    }

    @Test
    public void CamJoinName3() throws ZKNamingException, SauronException {
        assertEquals(camName3, frontend.camJoin("abc", 10.000, 10.000));
    }

    @Test
    public void CamJoinName15() throws ZKNamingException, SauronException {
        assertEquals(camName15, frontend.camJoin("123456789abcdef", 10.000, 10.000));
    }

    @Test
    public void CamJoinName16() throws ZKNamingException, SauronException {
        assertEquals(INVALID_CAMERA_NAME.message,
                assertThrows(SauronException.class, () -> frontend.camJoin("123456789abcdefg", 10.000, 10.000))
                        .getMessage());
    }

	//Test lat

	@Test
	public void CamJoinLat90() throws ZKNamingException, SauronException {
		assertEquals(camLat90, frontend.camJoin("Latitude90", 90.0, 0.0));
	}

	@Test
	public void CamJoinLat91() throws ZKNamingException, SauronException {
		assertEquals(INVALID_COORDINATES.message,
                assertThrows(SauronException.class, () -> frontend.camJoin("Latitude91", 91.0, 0.0))
						.getMessage());
	}

	@Test
	public void CamJoinLatNeg90() throws ZKNamingException, SauronException {
		assertEquals(camLatNeg90, frontend.camJoin("LatitudeNeg90", -90.0, 0.0));
	}

	@Test
	public void CamJoinLatNeg91() throws ZKNamingException, SauronException {
		assertEquals(INVALID_COORDINATES.message,
                assertThrows(SauronException.class, () -> frontend.camJoin("LatitudeNeg91", -91.0, 0.0))
						.getMessage());
	}

	
	//Test long
	@Test
	public void CamJoinLong180() throws ZKNamingException, SauronException {
		assertEquals(camLong180, frontend.camJoin("Longitude180", 0.0, 180.0));
	}

	@Test
	public void CamJoinLong181() throws ZKNamingException, SauronException {
		assertEquals(INVALID_COORDINATES.message,
                assertThrows(SauronException.class, () -> frontend.camJoin("Longitude181", 0.0, 181.0))
						.getMessage());
	}
	

	@Test
	public void CamJoinLongNeg180() throws ZKNamingException, SauronException {
		assertEquals(camLongNeg180, frontend.camJoin("LongitudeNeg180", 0.0, -180.0));
	}

	@Test
	public void CamJoinLongNeg181() throws ZKNamingException, SauronException {
		assertEquals(INVALID_COORDINATES.message,
                assertThrows(SauronException.class, () -> frontend.camJoin("LongitudeNeg181", 0.0, -181.0))
						.getMessage());
	}

	//other tests

	@Test
	public void DUplicateCamJoinSameCoords() throws ZKNamingException, SauronException {
		assertEquals(duplicateCam, frontend.camJoin("Duplicate", 10.000, 10.000));
	}

	@Test
    public void DuplicateCamJoinDiffCoords() throws ZKNamingException, SauronException {
        frontend.camJoin("TestCameraDup", 5.000, 7.500);
        assertEquals(INVALID_CAMERA.message,
                assertThrows(SauronException.class, () -> frontend.camJoin("TestCameraDup", 10.000, 15.000))
                        .getMessage());
    }
}
