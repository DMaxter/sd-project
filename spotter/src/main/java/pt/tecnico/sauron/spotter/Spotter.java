package pt.tecnico.sauron.spotter;

import pt.tecnico.sauron.silo.lib.Observation;
import pt.tecnico.sauron.silo.lib.SauronException;
import pt.tecnico.sauron.silo.client.SiloFrontend;
import pt.tecnico.sauron.silo.lib.Type;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;

import static pt.tecnico.sauron.silo.lib.SauronMessage.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Spotter {
    private SiloFrontend frontend;

    Spotter(String host, String port, String path, String instance, int cacheSize) throws ZKNamingException {
        this.frontend = new SiloFrontend(host, port, path, instance, cacheSize);
    }

    public void spot(String[] args) throws ZKNamingException, SauronException {
        if (args.length != 2) {
            throw new SauronException(WRONG_USAGE);
        }

        Type type;
        try {
            type = Type.valueOf(args[0]);
        } catch (NullPointerException | IllegalArgumentException e) {
            throw new SauronException(UNKNOWN_TYPE);
        }

        Pattern pattern = Pattern.compile(type.partial);
        Matcher matcher = pattern.matcher(args[1]);

        if (!matcher.matches()) {
            throw new SauronException(INVALID_ID);
        }

        List<Observation> observations = this.frontend.spot(type, args[1]);

        observations.sort(null);

        for (Observation o : observations) {
            System.out.println(o);
        }
    }

    public void trail(String[] args) throws ZKNamingException, SauronException {
        if (args.length != 2) {
            throw new SauronException(WRONG_USAGE);
        }

        Type type;

        try {
            type = Type.valueOf(args[0]);
        } catch (NullPointerException | IllegalArgumentException e) {
            throw new SauronException(UNKNOWN_TYPE);
        }

        Pattern pattern = Pattern.compile(type.regex);
        Matcher matcher = pattern.matcher(args[1]);

        if (!matcher.matches()) {
            throw new SauronException(INVALID_ID);
        }

        try {
            List<Observation> observations = this.frontend.trail(type, args[1]);

            for (Observation o : observations) {
                System.out.println(o);
            }
        } catch (NullPointerException e) {
            System.out.printf("Nothing found with type \"%s\" and id \"%s\"%n", type, args[1]);
        }
    }

    public void ping(String name) throws SauronException {
        System.out.println(frontend.ctrl_ping(name));
    }

    public void info(String camName) throws ZKNamingException, SauronException {
        System.out.println(frontend.camInfo(camName));
    }

    public void clear() throws ZKNamingException, SauronException {
        if (!frontend.ctrl_clear()) {
            throw new SauronException(OPERATION_NOT_COMPLETED);
        }

        System.out.println("Server state cleared");
    }

    public void close() {
        frontend.close();
    }
}
