# Guião de Demonstração

## 1. Preparação do sistema
Para testar a aplicação e todos os seus componentes, é necessário preparar um ambiente com dados para proceder à verificação dos testes.

### 1.1. Compilar o Projeto
Primeiramente, é necessário instalar as dependências necessárias para o silo e os clientes (eye e spotter) e compilar estes componentes. Para isso, basta ir à diretoria root do projeto e correr o seguinte comando:

$ mvn clean install -DskipTests

Com este comando já é possível analisar se o projeto compila na íntegra.

### 1.2. Silo
Para proceder aos testes, é preciso o servidor silo estar a correr. Para isso basta ir à diretoria silo-server e executar:

$ mvn exec:java

Este comando vai colocar o silo no endereço localhost e na porta 8081.

### 1.3. Eye
Vamos registar 3 câmeras e as respetivas observações. Cada câmera vai ter o seu ficheiro de entrada próprio com observações já definidas. Para isso basta ir à diretoria eye e correr os seguintes comandos:

```
$ eye localhost 2181 /grpc/sauron/silo Tagus 38.737613 -9.303164 < ../demo/eye1.txt

$ eye localhost 2181 /grpc/sauron/silo Alameda 30.303164 -10.737613 < ../demo/eye2.txt

$ eye localhost 2181 /grpc/sauron/silo Lisboa 32.737613 -15.303164 < ../demo/eye3.txt
```

Nota: Para correr o cliente **eye** ver [2.2](#22-inicializa%c3%a7%c3%a3o).

Depois de executar os comandos acima já temos o que é necessário para [testar o sistema](#4-teste-das-opera%c3%a7%c3%b5es) mas primeiro daremos uma pequena demonstração dos clientes [eye](#2-eye) e [spotter](#3-spotter)

## 2. Eye

###  **2.1 Argumentos Iniciais:**
O eye não será inicializado sem estes argumentos:
- \<**host**>: host do servidor
- \<**port**>: porto do servidor
- \<**path**>: caminho onde se encontra o servidor de nomes
- \<**name**>: nome da camera que irá reportar ao servidor
  - deve ser alfanumérico com comprimento mínimo de 3 e máximo de 15 caracteres
- \<**latitude**>: latitude da camera
  - deve estar compreendida entre -90 e 90
- \<**longitude**>: longitude da camera
  - deve estar compreendida entre -180 e 180

Opcional:
- \<**instance**>: réplica a ligar (se omitido, é atribuida uma aleatória)


### **2.2 Inicialização**
Na diretoria **eye**:

Pode definir a PATH para poder correr diretamente o script **eye**:

    $ PATH=target/appassembler/bin:$PATH

Após mudar a PATH, poderá iniciar o **eye** da seguinte forma:
  
    $ eye <host> <port> <path> <name> <latitude> <longitude> [<instance>]

Para correr através do Maven (com valores padrão):

    $ mvn exec:java 

Os argumentos iniciais podem ser alterados adicionando no final:
- -Dzoo.host=\<**host**> 
- -Dzoo.port=\<**port**> 
- -Dpath=\<**path**> 
- -Dcam.name=\<**name**> 
- -Dcam.latitude=\<**latitude**> 
- -Dcam.longitude=\<**longitude**>
- -Dinstance=\<**instance**>

Nota: É possível enviar o conteúdo de um ficheiro para o programa, com o operador de redirecionamento ( < data.txt, por exemplo).

### **2.3 Funcionalidades**

#### **Envio de informação**

    <tipo>, <id>

\<**tipo**>: tipo da entidade a reportar

\<**id**>: id da entidade a reportar

Podem ser submetidas várias observações seguidas.O seu envio só é feito após uma linha vazia ou caso o standard input seja fechado.

##### **Exemplos**
Submeter a observação de uma pessoa:

    person,5026726351

Submeter a observação de um veículo:

    car,20SD20

Submeter várias observações:

    car,20SD21
    person,108735282
    car,20SD22

#### **Comentário**

    # <texto>

Não regista nenhum input após o simbolo **#**

#### **Pausa**

    zzz, <n>

Pausa o envio de input ao eye durante \<n> milissegundos


### **2.4 Casos de Utilização**
##### **2.4.1 Ficheiro Dados**

##### Inicialização (exemplo)

    $ eye localhost 2181 /grpc/sauron/silo Tecnico 34.933 -120.77769 < ../demo/eye.txt

##### Explicação passo a passo (eye.txt)

1. Recebe comentário e ignora
    ```
    # lote 1
    ```
2. Guarda pessoa no buffer de observações a serem enviadas
    ```
    person, 123456
    ```
3. Guarda carro no buffer de observações a serem enviadas

    ```
    car, 33EE44
    ```
4. Guarda pessoa no buffer de observações a serem enviadas
    ```
    person, 9876543
    ```
5. Pausa o tratamento de input durante 5 segundos
    ```
    zzz, 5000
    ```
6. Linha vazia manda as observações anteriormente guardadas
    ```
    ```
7. Recebe comentário e ignora
    ```
    # lote 2
    ```
8. Guarda pessoa no buffer de observações a serem enviadas
    ```
    person, 678922
    ```
9. Guarda carro no buffer de observações a serem enviadas
    ```
    car, 76TT77
    ```
10. Pausa o tratamento de input durante 1 segundo
    ```
    zzz, 1000
    ```
11. End of File manda as observações anteriormente guardadas


##### **2.4.2 Outros Exemplos**

##### Inicializações Incorretas

###### Nome da camera incorreto

    $ eye localhost 2182 /grpc/sauron/silo ab 10.0 10.0 
    Error: Camera name must have between 3-15 characters
        
    $ eye localhost 2182 /grpc/sauron/silo abdefgijklmnop 10.0 10.0 
    Error: Camera name must have between 3-15 characters

###### Argumentos inválidos
    $ eye errado
    Error: Wrong usage

##### Inicialização

    $ eye localhost 2181 /grpc/sauron/silo Tecnico 34.933 -120.77769

    EyeApp
    Saved: localhost:8080
    Registering camera: Tecnico (34.933000,-120.777690)

##### Tentativa de envio de carro com id inválido

    > car, 123456
    Error: Invalid id for given type

##### Tentativa de envio de tipo inexistente

    > train, 123456
    Error: Unknown type

##### Input inválido

    > random
    Error: Wrong usage


## 3. Spotter

###  **3.1 Argumentos Iniciais (obrigatórios):**
O spotter não será inicializado sem estes argumentos:
- \<**host**>: host do servidor
- \<**port**>: porto do servidor
- \<**path**>: caminho para o servidor de nomes
- \<**cacheSize**>: set size for client cache

Opcional:
- \<**instance**>: réplica a ligar (se omitido, é atribuida uma aleatória)


### **3.2 Inicialização**
Na diretoria **spotter**:

Pode definir a PATH para poder correr diretamente o script **spotter**:

    $ PATH=target/appassembler/bin:$PATH

Após mudar a PATH, poderá iniciar o **spotter** da seguinte forma:
  
     $ spotter <host> <port> <path> <cacheSize> [<instance>]

Para correr através do Maven (com valores padrão):

    $ mvn exec:java 

Os argumentos iniciais podem ser alterados adicionando no final:
- -Dzoo.host=\<**host**> 
- -Dzoo.port=\<**port**> 
- -Dpath=\<**path**> 
- -DcacheSize=\<**cacheSize**> 
- -Dinstance=\<**instance**>

Nota: É possível enviar o conteúdo de um ficheiro para o programa, com o operador de redirecionamento ( < data.txt, por exemplo).

### **3.3 Funcionalidades**

##### Comando spot
Procura a observação do objeto ou pessoa com o identificador ou fragmento de identificador.

    > spot <type> <id>

\<**tipo**>: tipo da entidade a reportar

\<**id**>: id da entidade a reportar

O resultado tem o seguinte formato:

    Tipo,Identificador,Data-Hora,Nome-Câmera,Latitude-Câmera,Longitude-Câmera

##### Comando trail
Procura o caminho percorrido pelo objeto ou pessoa, com o identificador exato, e com resultados ordenados da observação mais recente para a mais antiga.

    > trail <type> <id>

O resultado tem o seguinte formato:

   Tipo,Identificador,Data-Hora,Nome-Câmera,Latitude-Câmera,Longitude-Câmera

##### Comando info
Procura as coordenadas da camera com o nome inserido

    > info <name>

O resultado tem o seguinte formato:

    (latitude,longitude)

##### Comando help
Dispõe ajuda no terminal

    > help

### **3.4 Casos de Utilização**

##### **3.4.1 Ficheiro Dados**

##### Inicialização (do spotter) utilizando o ficheiro spotter.txt (exemplo)

    $ spotter localhost 2181 /grpc/sauron/silo  < ../demo/spotter.txt

##### Explicação passo a passo (spotter.txt)

1. Pesquisa por pessoa com ID 123456
    ```
    spot person 123456
    ```
2. É mostrada a observação mais recente dessa pessoa

3. Pesquisa por carro com ID 33EE44
    ```
    spot car 33EE44
    ```
4. É mostrada a observação mais recente desse carro

5. Pesquisa por pessoa com ID parcial
    ```
    spot person 12*
    ```
6. É mostrada a observação mais recente, ordenada por ID, para cada pessoa em que o ID começa por 12
7. Pesquisa por carro com ID parcial
    ```
    spot car *7
    ```
8. É mostrada a observação mais recente, ordenada por ID, para cada carro em que o ID acaba em 7
9. Listar observações da pessoa com ID 9876543
    ```
    trail person 9876543
    ```
10. É mostrada a lista de todas as observações de uma pessoa ordenadas da mais recente para a mais antiga
11. Listar observações do carro com ID 76TT77
    ```
    trail car 76TT77
    ```
12. É mostrada a lista de todas as observações de um carro ordenadas da mais recente para a mais antiga
13. Pedir informações sobre a camera com o nome 'Tecnico'
    ```
    info Tecnico
    ```
14. É mostrado as coordenadas da camera


##### **3.4.2 Outros Exemplos**

##### Inicialização Incorreta

###### Argumentos inválidos
    $ spotter errado
    Error: Wrong usage


## 4. SiloServer

### Sincronização

  No servidor utilizámos `ConcurrentHashMap` para guardar as entidades de domínio, de forma a permitir a escrita e leitura concorrentes nesta estrutura. Optámos também por utilizar `CopyOnWriteArrayList` para guardar as observações dentro de cada entidade, de forma a que possível seja manipular também esta entidade de forma concorrente.
  
  No caso de ser utilizada a chamada remota `ctrl_clear`, os dados não são perdidos nas threads que estão ativas, uma vez que criamos novos mapas nesta operação, deixando os antigos para o Garbage Collector remover.
  
  Os possíveis problemas que podiam ser encontrados residiam no facto de termos 2 threads ou a tentar adicionar elementos ao HashMap ou 2 threads a tentar adicionar uma observação para a mesma pessoa, visto não haver remoção nem de 1 pessoa nem de 1 observação. No entanto, como as estruturas de dados estão preparadas para leituras e escritas concorrentes. Existem ainda outros casos, como o caso de uma thread estar a ver as observações de um objeto de domínio e outra estar a adicionar no mesmo instante, mas consideramos que não existe grande problema em, neste caso, não mostrarmos as observações registadas no instante em que a thread está a ler. Assim como, na tentativa de aceder a um objeto de domínio que está a ser criado no momento da pesquisa.



## 5. Teste das Operações (Discussão)

Nesta secção vamos correr os comandos necessários para testar todas as operações na discussão do projeto. 
Cada subsecção é respetiva a cada operação presente no *silo*.

#### 5.1. *cam_join*

Esta operação já foi testada na preparação do ambiente, no entanto ainda é necessário testar algumas restrições.

##### 5.1.1. Teste das câmeras com nome duplicado e coordenadas diferentes.  
O servidor deve rejeitar esta operação. 
Para isso basta executar um *eye* com o seguinte comando:

```
$ eye localhost 2181 /grpc/sauron/silo Tagus 10.0 10.0
Error: Camera already exists with different coordinates
```

##### 5.1.2. Teste do tamanho do nome.  
O servidor deve rejeitar esta operação. 
Para isso basta executar um *eye* com o seguinte comando:

```
$ eye localhost 2181 /grpc/sauron/silo ab 10.0 10.0
Error: Camera name must have between 3-15 characters
$ eye localhost 2181 /grpc/sauron/silo abcdefghijklmnop 10.0 10.0
Error: Camera name must have between 3-15 characters
```

#### 5.2. *cam_info*

Esta operação não tem nenhum comando específico associado e para isso é necessário ver qual o nome do comando associado a esta operação. 
Para isso precisamos instanciar um cliente *spotter*, presente na diretoria com o mesmo nome:

```
$ mvn exec:java
```

De seguida, corremos o comando *help*:
```
> help
```

##### 5.2.1. Teste para uma câmera existente.  
O servidor deve responder com as coordenadas de localização da câmera *Tagus* (38.737613 -9.303164):

```
> info Tagus
```

##### 5.2.2. Teste para câmera inexistente.  
O servidor deve rejeitar esta operação:

```
> info Inexistente
camInfo: Unregistered camera
```

#### 5.3. *report*

Esta operação já foi testada acima na preparação do ambiente.

#### 5.4. *track*

Esta operação vai ser testada utilizando o comando *spot* com um identificador.

##### 5.4.1. Teste com uma pessoa (id inexistente):

```
> spot person 14388236
spot: Couldn't find id
```

##### 5.4.2. Teste com uma pessoa:

```
> spot person 123456789
person,123456789,<timestamp>,Alameda,30.303164,-10.737613
```

##### 5.4.3. Teste com um carro:

```
> spot car 20SD21
car,20SD21,<timestamp>,Alameda,30.303164,-10.737613
```

#### 5.5. *trackMatch*

Esta operação vai ser testada utilizando o comando *spot* com um fragmento de identificador.

##### 5.5.1. Teste com uma pessoa (id inexistente):

```
> spot person 143882*
spot: Couldn't find id
```

##### 5.5.2. Testes com uma pessoa:

```
> spot person 111*
person,111111000,<timestamp>,Tagus,38.737613,-9.303164

> spot person *000
person,111111000,<timestamp>,Tagus,38.737613,-9.303164

> spot person 111*000
person,111111000,<timestamp>,Tagus,38.737613,-9.303164
```

##### 5.5.3. Testes com duas ou mais pessoas:

```
> spot person 123*
person,123111789,<timestamp>,Alameda,30.303164,-10.737613
person,123222789,<timestamp>,Alameda,30.303164,-10.737613
person,123456789,<timestamp>,Alameda,30.303164,-10.737613

> spot person *789
person,123111789,<timestamp>,Alameda,30.303164,-10.737613
person,123222789,<timestamp>,Alameda,30.303164,-10.737613
person,123456789,<timestamp>,Alameda,30.303164,-10.737613

> spot person 123*789
person,123111789,<timestamp>,Alameda,30.303164,-10.737613
person,123222789,<timestamp>,Alameda,30.303164,-10.737613
person,123456789,<timestamp>,Alameda,30.303164,-10.737613
```

##### 5.5.4. Testes com um carro:

```
> spot car 00A*
car,00AA00,<timestamp>,Tagus,38.737613,-9.303164

> spot car *A00
car,00AA00,<timestamp>,Tagus,38.737613,-9.303164

> spot car 00*00
car,00AA00,<timestamp>,Tagus,38.737613,-9.303164
```

##### 5.5.5. Testes com dois ou mais carros:

```
> spot car 20SD*
car,20SD20,<timestamp>,Alameda,30.303164,-10.737613
car,20SD21,<timestamp>,Alameda,30.303164,-10.737613
car,20SD22,<timestamp>,Alameda,30.303164,-10.737613

> spot car *XY20
car,66XY20,<timestamp>,Lisboa,32.737613,-15.303164
car,67XY20,<timestamp>,Alameda,30.303164,-10.737613
car,68XY20,<timestamp>,Tagus,38.737613,-9.303164

> spot car 19SD*9
car,19SD19,<timestamp>,Lisboa,32.737613,-15.303164
car,19SD29,<timestamp>,Lisboa,32.737613,-15.303164
car,19SD39,<timestamp>,Lisboa,32.737613,-15.303164
car,19SD49,<timestamp>,Lisboa,32.737613,-15.303164
car,19SD59,<timestamp>,Lisboa,32.737613,-15.303164
car,19SD69,<timestamp>,Lisboa,32.737613,-15.303164
car,19SD79,<timestamp>,Lisboa,32.737613,-15.303164
car,19SD89,<timestamp>,Lisboa,32.737613,-15.303164
car,19SD99,<timestamp>,Lisboa,32.737613,-15.303164
```

#### 5.6. *trace*

Esta operação vai ser testada utilizando o comando *trail* com um identificador.

##### 5.6.1. Teste com uma pessoa (id inexistente):

```
> trail person 14388236
trail: Couldn't find id
```

##### 5.6.2. Teste com uma pessoa:

```
> trail person 123456789
person,123456789,<timestamp>,Alameda,30.303164,-10.737613
person,123456789,<timestamp>,Alameda,30.303164,-10.737613
person,123456789,<timestamp>,Tagus,38.737613,-9.303164

```

##### 5.6.3. Teste com um carro (id inexistente):

```
> trail car 12XD34
trail: Couldn't find id
```

##### 5.6.4. Teste com um carro:

```
> trail car 00AA00
car,00AA00,<timestamp>,Tagus,38.737613,-9.303164
car,00AA00,<timestamp>,Tagus,38.737613,-9.303164
```


## 6. Replicação e Tolerância a Faltas

### 6.1 Lançar replicas
As réplicas são inicializadas na diretoria **silo-server** e dispõem de vários agumentos iniciais configuráveis.

Quando uma réplica recebe *gossip* de outra, dispõe a seguinte mensagem (exemplo):

    GOSSIP RECEIVED FROM REPLICA 2

Quando uma réplica manda um update a outra, dispõe a seguinte mensagem (exemplo):

    GOSSIPING WITH localhost:8081 1 UPDATES



##### 6.1.1 Argumentos Iniciais

- \<**zooHost**>: host do servidor ZooKeeper
- \<**zooPort**>: porto do servido Zookeeper
- \<**host**>: host do servidor
- \<**port**>: porto do servidor
- \<**path**>: caminho onde se encontra o servidor de nomes
- \<**instance**>: número identificador da réplica
- \<**timer**>: intervalo de tempo entre cada comunicação entre réplicas 


##### 6.1.2 Inicialização
Na diretoria **silo-server**:

    $ mvn exec:java

Os argumentos iniciais podem ser alterados adicionando no final:
- -Dzoo.host=\<**zooHost**> 
- -Dzoo.port=\<**zooPort**>
- -Dserver.host=\<**host**> 
- -Dserver.port=\<**port**> 
- -Dpath=\<**path**> 
- -Dinstance=\<**instance**> (se omitido, é atribuído o valor 1)
- -Dtimer=\<**timer**> (se omitido, o timer é igual a 30 segundos)

### 6.2 Fornecer dados
Utilizando o cliente **eye** como exemplo, ligaremos primeiro a réplica 2:

    $ mvn exec:java -Dinstance=2

De seguida, ligamos o cliente **eye** e especificamos a réplica a ser atribuida ao cliente:

     $ eye localhost 2181 /grpc/sauron/silo Tagus 38.737613 -9.303164 2

Ou:

    $ mvn exec:java -Dcam.name=Tagus -Dcam.lat=38.737613 -Dcam.long=-9303164 -Dinstance=2

Agora ligado à réplica 2, o cliente **eye** poderá realizar as suas operações, descritas [acima](#2-eye).

### 6.3 Tolerância a faltas (Discussão)
Aqui encontram-se alguns exemplos de casos de utilização a tolerar faltas para mostrar na discussão:

#### 6.3.1 Réplica *crasha*
Neste caso, o cliente procede com a verificação das réplicas que se encontram ativas, sendo atribuida uma réplica que se encontre disponível.

1. Ligar réplica 1 (diretoria **silo-server**):
    ```
    $ mvn exec:java
    Silo Server Initializing...
    Running on port 8081
    Gossip every 30 seconds
    Replica 1 started
    Press <ENTER> to shutdown
    ```
2. Ligar réplica 2 (diretoria **silo-server**):
    ```
    $ mvn exec:java -Dinstance=2
    Silo Server Initializing...
    Running on port 8082
    Gossip every 30 seconds
    Replica 2 started
    Press <ENTER> to shutdown
    ```
3. Ligar cliente **eye** à réplica 2 (diretoria **eye**):
    ```
    $ eye localhost 2181 /grpc/sauron/silo Tagus 38.737613 -9.303164 2
    EyeApp
    Registering camera: Tagus (38.737613,-9.303164)
    ```
4. Verificar mensagem de confirmação na réplica 2:
    ```
    CAMERA "Tagus" CREATED WITH COORDINATES (38.737613,-9.303164)
    ```
5. Reportar uma pessoa no **eye**:
    ```
    > person, 1
    > 
    Sending to camera: Tagus
    - person, 1
    ```
6. Verificar mensagem de confirmação na réplica 2:
    ```
    REPORTED 1 OBSERVABLE ENTITIES
    ```
7. Desligar réplica 2:
    ```
    $ \n
    ```
8. Reportar um carro no **eye**:
    ```
    > car, 11SS22
    > 
    Sending to camera: Tagus
    - car, 11SS22
    ```
9. Verificar que a réplica a servir o cliente eye é agora a réplica 1, vendo as suas mensagens de confirmação:
    ```
    CAMERA "Tagus" CREATED WITH COORDINATES (38.737613,-9.303164)
    REPORTED 1 OBSERVABLE ENTITIES
    ```

#### 6.3.2 Réplicas só fazem *gossip* com réplicas ativas
1. Ligar réplica 1 (diretoria **silo-server**):
    ```
    $ mvn exec:java -Dtimer=5000
    Silo Server Initializing...
    Running on port 8081
    Gossip every 5 seconds
    Replica 1 started
    Press <ENTER> to shutdown
    ```
2. Ligar réplica 2 (diretoria **silo-server**):
    ```
    $ mvn exec:java -Dinstance=2 -Dtimer=10000
    Silo Server Initializing...
    Running on port 8082
    Gossip every 10 seconds
    Replica 2 started
    Press <ENTER> to shutdown
    ```
3. Ligar réplica 3 (diretoria **silo-server**):
    ```
    $ mvn exec:java -Dinstance=3 -Dtimer=15000
    Silo Server Initializing...
    Running on port 8082
    Gossip every 15 seconds
    Replica 3 started
    Press <ENTER> to shutdown
    ```
4. Verificar as mensagens recebidas em cada réplica. Veremos o caso da réplica 1:
   
    A réplica 1 manda, a cada 5 segundos, os seus updates para as réplicas 2 e 3:
   
        GOSSIPING WITH localhost:8082 0 UPDATES
        GOSSIPING WITH localhost:8083 0 UPDATES
   
    Recebe, a cada 10 segundos, os updates da réplica 2:

        GOSSIP RECEIVED FROM REPLICA 2
    
    Recebe, a cada 15 segundos, os updates da réplica 3:

        GOSSIP RECEIVED FROM REPLICA 3

5. Desligar réplica 3:
    ```
    $ \n
    ```
6. Verificar que agora a réplica 1 só comunica com a réplica 2, e vice-versa:

    A réplica 1 manda, a cada 5 segundos, os seus updates para as réplicas 2:
   
        GOSSIPING WITH localhost:8082 0 UPDATES
   
    Recebe, a cada 10 segundos, os updates da réplica 2:

        GOSSIP RECEIVED FROM REPLICA 2

#### 6.3.3 Cliente tentar conectar a uma réplica que crashou e voltou a funcionar
1. Ligar réplica 1 (diretoria **silo-server**):
    ```
    $ mvn exec:java
    Silo Server Initializing...
    Running on port 8081
    Gossip every 30 seconds
    Replica 1 started
    Press <ENTER> to shutdown
    ```
2. Ligar réplica 2 (diretoria **silo-server**):
    ```
    $ mvn exec:java -Dinstance=2
    Silo Server Initializing...
    Running on port 8082
    Gossip every 30 seconds
    Replica 2 started
    Press <ENTER> to shutdown
    ```
3. Ligar cliente **eye** à réplica 2 (diretoria **eye**):
    ```
    $ eye localhost 2181 /grpc/sauron/silo Tagus 38.737613 -9.303164 2
    EyeApp
    Registering camera: Tagus (38.737613,-9.303164)
    ```
4. Verificar mensagem de confirmação na réplica 2:
    ```
    CAMERA "Tagus" CREATED WITH COORDINATES (38.737613,-9.303164)
    ```
5. Desligar réplica 2:
    ```
    $ \n
    ```
6. Voltar a ligar réplica 2 (diretoria **silo-server**):
    ```
    $ mvn exec:java -Dinstance=2
    Silo Server Initializing...
    Running on port 8082
    Gossip every 30 seconds
    Replica 2 started
    Press <ENTER> to shutdown
    ``` 
7. Reportar uma pessoa no **eye**:
    ```
    > person, 1
    > 
    Sending to camera: Tagus
    - person, 1
    ```
8. Verificar mensagem de confirmação na réplica 2:

    Antes de conseguir enviar a observação, o cliente apercebe-se do erro e recolhe informações acerca da camera, para a réplica a voltar a registar:
    ```
    TRIED TO REPORT 1 OBSERVATIONS EXCEPTION:Found 1 invalid observation(s)
    GOT INFO FROM CAMERA WITH NAME Tagus
    REPORTED 1 OBSERVABLE ENTITIES
    CAMERA "Tagus" CREATED WITH COORDINATES (38.737613,-9.303164)
    ```
