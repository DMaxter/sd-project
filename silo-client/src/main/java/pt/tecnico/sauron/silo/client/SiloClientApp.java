package pt.tecnico.sauron.silo.client;

import io.grpc.StatusRuntimeException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import pt.tecnico.sauron.silo.grpc.*;
import pt.tecnico.sauron.silo.lib.*;
import pt.ulisboa.tecnico.sdis.zk.ZKNamingException;

import static pt.tecnico.sauron.silo.lib.SauronMessage.*;

public class SiloClientApp {

    public static void main(String[] args) throws IOException, SauronException, ZKNamingException {
        System.out.println(SiloClientApp.class.getSimpleName());

        // receive and print arguments
        System.out.printf("Received %d arguments%n", args.length);
        for (int i = 0; i < args.length; i++) {
            System.out.printf("arg[%d] = %s%n", i, args[i]);
        }

        String host = args[0];
        String port = args[1];
        String path = args[2];
        String instance = args[3];
        int cacheSize = Integer.parseInt(args[4]);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.print("> ");
            String input = reader.readLine();

            if (input.trim().length() == 0) {
                System.out.println();
                continue;
            }

            try {
                parse(input, host, port, path, instance, cacheSize);
            } catch (NullPointerException e) {
                System.out.println();
                break;
            } catch (SauronException e) {
                System.out.println(e.getMessage());
            }

        }
    }

    private static void parse(String input, String host, String port, String path, String instance, int cacheSize) throws SauronException, ZKNamingException {
        String[] parsed = input.split("\\s+");
        String[] args = Arrays.copyOfRange(parsed, 1, parsed.length);

        SiloFrontend frontend = new SiloFrontend(host, port, path, instance, cacheSize);

        switch (parsed[0]) {
            case "spot":
                spot(frontend, args);
                break;
            case "trail":
                trail(frontend, args);
                break;
            case "ping":
                ping(frontend, args);
                break;
            case "clear":
                clear(frontend);
                break;
            case "init":
                init(frontend);
                break;
            case "help":
                usage();
                break;
            default:
                System.out.println("Unknown command");
                usage();
        }

        frontend.close();
    }

    private static void usage(){
        System.out.println("Usage:");
        System.out.println("\thelp - This help");
        System.out.println("\tping - Check server status");
        System.out.println("\tclear - Clean server state");
        System.out.println("\tspot <type> <id | id fragment with *> - Search id/fragment for given type");
        System.out.println("\ttrail <type> <id> - Get full trace of the type with given id");
    }

    public static void spot(SiloFrontend frontend, String[] args) throws ZKNamingException, SauronException {
        if (args.length != 2) {
            throw new SauronException(WRONG_USAGE);
        }

        Type type;
        try {
            type = Type.valueOf(args[0]);
        } catch (NullPointerException | IllegalArgumentException e) {
            throw new SauronException(UNKNOWN_TYPE);
        }

        try {
            List<Observation> observations = frontend.spot(type, args[1]);

            for (Observation o : observations) {
                System.out.println(o);
            }
        } catch (NullPointerException e) {
            System.out.printf("Nothing found with type \"%s\" and id \"%s\"%n", type, args[1]);
        }
    }

    public static void trail(SiloFrontend frontend, String[] args) throws ZKNamingException, SauronException {
        if (args.length != 2) {
            throw new SauronException(WRONG_USAGE);
        }

        Type type;

        try {
            type = Type.valueOf(args[0]);
        } catch (NullPointerException | IllegalArgumentException e) {
            throw new SauronException(UNKNOWN_TYPE);
        }

        try {
            List<Observation> observations = frontend.trail(type, args[1]);

            for (Observation o : observations) {
                System.out.println(o);
            }
        } catch (NullPointerException e) {
            System.out.printf("Nothing found with type \"%s\" and id \"%s\"%n", type, args[1]);
        }
    }

    public static void ping(SiloFrontend frontend, String[] args) throws ZKNamingException, SauronException {
        if (args.length != 1) {
            throw new SauronException(WRONG_USAGE);
        }

        System.out.println(frontend.ctrl_ping(args[0]));
    }

    public static void clear(SiloFrontend frontend) throws ZKNamingException, SauronException {
        if (!frontend.ctrl_clear()) {
            throw new SauronException(OPERATION_NOT_COMPLETED);
        }

        System.out.println("Server state cleared");
    }

    public static void init(SiloFrontend frontend) throws ZKNamingException, SauronException {
        if (!frontend.ctrl_init()) {
            throw new SauronException(OPERATION_NOT_COMPLETED);
        }

        System.out.println("Server populated with cameras and observables");
    }
}
