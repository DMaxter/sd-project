package pt.tecnico.sauron.silo.domain;

import pt.tecnico.sauron.silo.lib.Type;

public class Person extends Observable {
    public Person(Type type, long id) {
        super(type, id);
    }
}
