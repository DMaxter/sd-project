# Silo lib

## About

This library module contains types and classes used by the server and that he makes available to the clients, if they wish to use it (mandatory to use SiloFrontend).

## Instruction for using Maven

Make sure the parent POM was installed first.

To compile and install:

```
mvn install
```

## To configure the Maven project in Eclipse

'File', 'Import...', 'Maven'-'Existing Maven Projects'

'Select root directory' and 'Browse' to the project base folder.

Check that the desired POM is selected and 'Finish'.


----
