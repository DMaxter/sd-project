package pt.tecnico.sauron.silo.lib;

public class InternalException extends Exception {
    private static final long serialVersionUID = 20200327L;
    public SauronMessage sauronMessage;

    public InternalException(SauronMessage message) {
        super(message.message);
        this.sauronMessage = message;
    }

    public InternalException(SauronMessage message, String text){
        super(message.message + text);
    }

    public InternalException(SauronMessage message, Integer num){
        super(String.format(message.message, num));
    }
}
